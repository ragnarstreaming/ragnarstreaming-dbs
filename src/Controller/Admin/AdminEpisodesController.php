<?php
namespace App\Controller\Admin;

use App\Form\EpisodeType;
use App\Form\SaisonType;
use App\Repository\EpisodeRepository;
use Curl\Curl;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Episode;
use App\Entity\Saison;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Flex\Response;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\File;

class AdminEpisodesController extends AbstractController
{
    /**
     * @var EpisodeRepository
     */
    private $repository;
    /**
     * @var ObjectManager
     */
    private $em;

    public function __construct(EpisodeRepository $repository, ObjectManager $em)
    {

        $this->repository = $repository;
        $this->em = $em;
    }

    /**
     * @Route("/admin-episodes", name="admin.episodes.index")
     */
    public function index()
    {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY'))
        {
            $episodes = $this->repository->findAll();
            return $this->render('admin/episodes/index.html.twig', compact('episodes'));
        }
        return $this->redirectToRoute('home');

    }

    /**
     * @Route("/admin/create-episode", name="admin.episode.create")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(Request $request)
    {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY'))
        {
            $episode = new Episode();
            $form = $this->createForm(EpisodeType::class, $episode);
            $form->handleRequest($request);
            if($form->isSubmitted() && $form->isValid())
            {

                $file = $episode->getAvatar();

                $fileName = $this->generateUniqueFileName().'.'.$file->guessExtension();

                try {
                    $file->move(
                        $this->getParameter('episodes_directory'),
                        $fileName
                    );

                } catch (FileException $e) {
                    $this->addFlash('error', $e->getMessage());
                    return $this->redirectToRoute("admin.episodes.index");
                }

                $episode->setAvatar($fileName);
                $this->em->persist($episode);
                $this->em->flush();
                $this->addFlash('success', 'Cet épisode a été ajouté!');
                return $this->redirectToRoute("admin.episodes.index");
            }

            return $this->render('admin/episodes/create.html.twig', [
                'episode' => $episode,
                'form' => $form->createView()
            ]);
        }

        return $this->redirectToRoute('home');
    }

    /**
     * @Route("/admin/edit-episode/{id}", name="admin.episode.edit", methods="GET|POST")
     * @param Episode $episode
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(Episode $episode, Request $request)
    {

        if ($this->isGranted('IS_AUTHENTICATED_FULLY'))
        {
            $episode->setAvatar(
                new File($this->getParameter('episodes_directory').'/'.$episode->getAvatar())
            );
            $filename = $episode->getAvatar()->getFilename();


            $form = $this->createForm(EpisodeType::class, $episode);
            $form->handleRequest($request);

            if($form->isSubmitted() && $form->isValid())
            {
                if($request->files->get('episode')["avatar"] !== null)
                {

                    $file = $episode->getAvatar();

                    $new_fileName = $this->generateUniqueFileName().'.'.$file->guessExtension();

                    try {
                        $file->move(
                            $this->getParameter('episodes_directory'),
                            $new_fileName
                        );
                    } catch (FileException $e) {
                        // ... handle exception if something happens during file upload
                    }

                    $episode->setAvatar($new_fileName);
                }
                if ($request->files->get('episode')["avatar"] === null)
                {
                    $episode->setAvatar($filename);
                }

                $this->em->persist($episode);
                $this->em->flush();
                $this->addFlash('success', 'Episode: ' . $episode->getTitle() . ' edité!');
                return $this->redirectToRoute("admin.episodes.index");
            }

            return $this->render('admin/episodes/edit.html.twig', [
                'episode' => $episode,
                'form' => $form->createView()
            ]);
        }
        return $this->redirectToRoute('home');

    }

    /**
     * @Route("/admin/edit-link-episode/{id}", name="admin.link.episode.edit", methods="GET|POST")
     * @param Episode $episode
     * @param Curl $curl
     * @return \Symfony\Component\HttpFoundation\Response
     * @internal param Request $request
     */
    public function editLink(Episode $episode)
    {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY'))
        {
            $episode_number = $episode->getNumber();
            $saison_number = $episode->getSaison()->getId();
            $model = "";
            $link_to_update = [];
            $links = [];
            $link_vostfr = "";
            $link_vf = "";
            $folder_saison_ids = [
                "S01" => "277599",
                "S02" => "277600",
                "S03" => "277601",
                "S04" => "277602",
                "S05" => "277604"];
            $folder_id = "";

            if($saison_number >= 10 && $episode_number < 10){
                $model = "#S" . $saison_number . "E" . $episode_number ."#";
                $folder_id = $folder_saison_ids["S" . $saison_number];
            }
            if($saison_number >= 10 && $episode_number >= 10){
                $model = "#S" . $saison_number . "E" . $episode_number ."#";
                $folder_id = $folder_saison_ids["S" . $saison_number];
            }
            if($saison_number < 10 && $episode_number >= 10){
                $model = "#S0" . $saison_number . "E" . $episode_number ."#";
                $folder_id = $folder_saison_ids["S0" . $saison_number];
            }
            if($saison_number < 10 && $episode_number < 10){
                $model = "#S0" . $saison_number . "E0" . $episode_number ."#";
                $folder_id = $folder_saison_ids["S0" . $saison_number];
            }

            $curl = new Curl();

            $fileInfo = $curl->get('https://doodapi.com/api/folder/list',[
                'key' => '6449trbpjc3kcir0rl0j',
                'fld_id' => $folder_id
            ]);
            $files = json_decode($fileInfo->getResponse(), true);
            $array_files = $files["result"]["files"];

            if(is_array($array_files) && count($array_files) > 0)
            {
                foreach ($array_files as $key => $value)
                {
                    $links[$value["title"]] = "https://dood.to/e/" . $value["file_code"];

                }
                if (count($links) > 0)
                {
                    foreach ($links as $key => $value)
                    {
                        if (preg_match($model, $key))
                        {
                            $link_to_update[$key] = $value;
                        }
                    }
                }
                if (count($link_to_update) > 0)
                {
                    foreach ($link_to_update as $key => $value)
                    {
                        if (preg_match("#VOSTFR#", $key))
                        {
                            $link_vostfr = $value;
                        }
                        if (preg_match("#VF#", $key)) {

                            $link_vf = $value;
                        }
                    }
                }
                if ($link_vostfr && $link_vf) {
                    if ($link_vostfr !== $episode->getOpenloadLinkVostfr() && $link_vf !== $episode->getOpenloadLinkVf()) {
                        $episode->setOpenloadLinkVf($link_vf);
                        $episode->setOpenloadLinkVostfr($link_vostfr);
                        $this->em->persist($episode);
                        $this->em->flush();
                        $this->addFlash('success', 'Les liens DOODSTREAM pour: ' . $episode->getSaison() . " EPISODE: " . $episode->getNumber() . " / " . $episode->getTitle() . ' ont été edités!');
                        return $this->redirectToRoute("admin.episodes.index");
                    } else {
                        $this->addFlash('warning', 'Les liens DOODSTREAM pour: ' . $episode->getSaison() . " EPISODE: " . $episode->getNumber() . " / " . $episode->getTitle() . ' sont déjà à jour!');
                        return $this->redirectToRoute("admin.episodes.index");
                    }
                }
            } else{
                $this->addFlash('error', 'Les liens DOODSTREAM pour: ' . strtoupper($episode->getSaison()) . " EPISODE: ". $episode->getNumber() . " / " .$episode->getTitle() . ' ont pas été edités! (too many request too many request vers API 3max each 2-3mins)');
                return $this->redirectToRoute("admin.episodes.index");
            }

        }
        return $this->redirectToRoute('home');
    }

    /**
     * @Route("/admin/edit-linkStream-episode/{id}", name="admin.link.episode.edit.stream", methods="GET|POST")
     * @param Episode $episode
     * @param Curl $curl
     * @return \Symfony\Component\HttpFoundation\Response
     * @internal param Request $request
     */
    public function editlinkStream(Episode $episode)
    {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY'))
        {
            $episode_number = $episode->getNumber();
            $saison_number = $episode->getSaison()->getId();
            $model = "";
            $link = [];
            $link_to_update = [];
            $link_vostfr = "";
            $link_vf = "";
            $folder_saison_ids = [
                "S01" => "131474",
                "S02" => "131475",
                "S03" => "131476",
                "S04" => "131477",
                "S05" => "131478",
                "S06" => "131479",
                "S07" => "131480",
                "S08" => "131481",];
            $folder_id = "";

            if($saison_number >= 10 && $episode_number < 10){
                $model = "#S" . $saison_number . "E" . $episode_number ."#";
                $folder_id = $folder_saison_ids["S" . $saison_number];
            }
            if($saison_number >= 10 && $episode_number >= 10){
                $model = "#S" . $saison_number . "E" . $episode_number ."#";
                $folder_id = $folder_saison_ids["S" . $saison_number];
            }
            if($saison_number < 10 && $episode_number >= 10){
                $model = "#S0" . $saison_number . "E" . $episode_number ."#";
                $folder_id = $folder_saison_ids["S0" . $saison_number];
            }
            if($saison_number < 10 && $episode_number < 10){
                $model = "#S0" . $saison_number . "E0" . $episode_number ."#";
                $folder_id = $folder_saison_ids["S0" . $saison_number];
            }

            $curl = new Curl();

            $fileInfo = $curl->get('https://api.vidoza.net/v1/files',[
                'api_token' => 'ePCDpbovRjOQI5D7viIklhD2qzmwQWEf4lSyIfafw1NS49SCwIkN73ZZIaRB',
                'folder_id' => $folder_id,
                'orderColumn' => 'name',
            ]);
            $files = json_decode($fileInfo->getResponse(), true);

            $array_files = $files["data"];
            if(is_array($array_files) && count($array_files) > 0)
            {
                foreach ($array_files as $key => $value)
                {
                    $completeLink = "https://vidoza.net/embed-" . $value["id"] . ".html" . "/" . $value["name"];
                    array_push($link, $completeLink);

                }
                if (count($link) > 0)
                {
                    foreach ($link as $key => $value)
                    {
                        if (preg_match($model, $value))
                        {
                            array_push($link_to_update, $value);
                        }
                    }
                    if (count($link_to_update) > 0)
                    {
                        foreach ($link_to_update as $key => $value)
                        {
                            if (preg_match("#VOSTFR#", $value))
                            {
                                $link_vostfr = $value;
                            }
                            if (preg_match("#VF#", $value))
                            {
                                $link_vf = $value;
                            }

                        }
                        if($link_vostfr)
                        {
                            $link_vostfr = explode("/", $link_vostfr);
                            $link_vostfr = array_slice($link_vostfr, 0, 4);
                            $link_vostfr = implode("/", $link_vostfr);
                        }
                        if($link_vf)
                        {
                            $link_vf = explode("/", $link_vf);
                            $link_vf = array_slice($link_vf, 0, 4);
                            $link_vf = implode("/", $link_vf);
                        }

                        if (strlen($link_vostfr) > 10 && strlen($link_vf) > 10)
                        {
                            //$link_vostfr = str_replace('/stream/', '/e/', $link_vostfr);
                            //$link_vf = str_replace('/stream/', '/e/', $link_vf);

                            if($link_vostfr !== $episode->getStreamangoLinkVostfr() && $link_vf !== $episode->getStreamangoLinkVf())
                            {
                                $episode->setStreamangoLinkVf($link_vf);
                                $episode->setStreamangoLinkVostfr($link_vostfr);
                                $this->em->persist($episode);
                                $this->em->flush();
                                $this->addFlash('success', 'Les liens VIDOZA pour: ' . $episode->getSaison(). " EPISODE: ". $episode->getNumber() . " / " .$episode->getTitle() . ' ont été edités!');
                                return $this->redirectToRoute("admin.episodes.index");
                            }
                            else{
                                $this->addFlash('warning', 'Les liens VIDOZA pour: ' . $episode->getSaison(). " EPISODE: ". $episode->getNumber() . " / " .$episode->getTitle() . ' sont déjà à jour!');
                                return $this->redirectToRoute("admin.episodes.index");
                            }
                        }
                    }
                }
            }
            else{
                $this->addFlash('error', 'Les liens VIDOZA pour: ' . strtoupper($episode->getSaison()) . " EPISODE: ". $episode->getNumber() . " / " .$episode->getTitle() . ' ont pas été edités! (too many request too many request vers API 3max each 2-3mins)');
                return $this->redirectToRoute("admin.episodes.index");
            }
        }
        return $this->redirectToRoute('home');
    }

    /**
     * @Route("/admin/delete-episode/{id}", name="admin.episode.delete", methods="DELETE")
     * @param Episode $episode
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function delete(Episode $episode, Request $request)
    {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY'))
        {
            if($this->isCsrfTokenValid("delete" . $episode->getId(), $request->get("_token")))
            {
                $this->em->remove($episode);
                $this->em->flush();
                $this->addFlash('success', 'Episode suprimé!');
            }
            return $this->redirectToRoute("admin.episodes.index");
        }
        return $this->redirectToRoute('home');

    }

    /**
     * @return string
     */
    private function generateUniqueFileName()
    {
        // md5() reduces the similarity of the file names generated by
        // uniqid(), which is based on timestamps
        return md5(uniqid());
    }
}
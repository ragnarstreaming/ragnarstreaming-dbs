<?php
namespace App\Controller\Admin;

use App\Form\SaisonType;
use App\Repository\SaisonRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Saison;
use App\Entity\Episode;
use App\Repository\EpisodeRepository;
use Curl\Curl;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Flex\Response;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\File;

class AdminSaisonsController extends AbstractController
{
    /**
     * @var SaisonRepository
     */
    private $repository;
    /**
     * @var ObjectManager
     */
    private $em;
    /**
     * @var EpisodeRepository
     */
    private $episodeRepository;

    public function __construct(SaisonRepository $repository, ObjectManager $em, EpisodeRepository $episodeRepository)
    {

        $this->repository = $repository;
        $this->em = $em;
        $this->episodeRepository = $episodeRepository;
    }

    /**
     * @Route("/admin-saisons", name="admin.saisons.index")
     */
    public function index()
    {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY'))
        {
            $saisons = $this->repository->findAll();
            return $this->render('admin/saisons/index.html.twig', compact('saisons'));
        }
        return $this->redirectToRoute('home');

    }

    /**
     * @Route("/admin/create-saison", name="admin.saison.create")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(Request $request)
    {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY'))
        {
            $saison = new Saison();

            $form = $this->createForm(SaisonType::class, $saison);
            $form->handleRequest($request);

            if($form->isSubmitted() && $form->isValid())
            {
                $file = $saison->getAvatar();

                $fileName = $this->generateUniqueFileName().'.'.$file->guessExtension();

                try {
                    $file->move(
                        $this->getParameter('saisons_directory'),
                        $fileName
                    );
                } catch (FileException $e) {
                    // ... handle exception if something happens during file upload
                    $this->addFlash('error', $e->getMessage());
                    return $this->redirectToRoute("admin.saisons.index");
                }

                $saison->setAvatar($fileName);
                $this->em->persist($saison);
                $this->em->flush();
                $this->addFlash('success', 'La saison a été ajoutée!');
                return $this->redirectToRoute("admin.saisons.index");
            }

            return $this->render('admin/saisons/create.html.twig', [
                'saison' => $saison,
                'form' => $form->createView()
            ]);
        }

        return $this->redirectToRoute('home');
    }

    /**
     * @Route("/admin/edit-saison/{id}", name="admin.saison.edit", methods="GET|POST")
     * @param Saison $saison
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(Saison $saison, Request $request)
    {

        if ($this->isGranted('IS_AUTHENTICATED_FULLY'))
        {
            $saison->setAvatar(
                new File($this->getParameter('saisons_directory').'/'.$saison->getAvatar())
            );
            $filename = $saison->getAvatar()->getFilename();
            $form = $this->createForm(SaisonType::class, $saison);
            $form->handleRequest($request);

            if($form->isSubmitted() && $form->isValid())
            {
                if($request->files->get('saison')["avatar"] !== null)
                {

                    $file = $saison->getAvatar();

                    $new_fileName = $this->generateUniqueFileName().'.'.$file->guessExtension();

                    try {
                        $file->move(
                            $this->getParameter('saisons_directory'),
                            $new_fileName
                        );
                    } catch (FileException $e) {
                        // ... handle exception if something happens during file upload
                    }

                    $saison->setAvatar($new_fileName);
                }
                if ($request->files->get('saison')["avatar"] === null)
                {
                    $saison->setAvatar($filename);
                }
                $this->em->persist($saison);
                $this->em->flush();
                $this->addFlash('success', 'La ' . $saison->getName() . ' a été editée!');
                return $this->redirectToRoute("admin.saisons.index");
            }

            return $this->render('admin/saisons/edit.html.twig', [
                'saison' => $saison,
                'form' => $form->createView()
            ]);
        }
        return $this->redirectToRoute('home');

    }

    /**
     * @Route("/admin/edit-link-saison/{id}", name="admin.link.saison.edit", methods="GET|POST")
     * @param Saison $saison
     * @param Curl $curl
     * @return \Symfony\Component\HttpFoundation\Response
     * @internal param Request $request
     */
    public function editLink(Saison $saison)
    {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY'))
        {
            $episodes_collection = $saison->getEpisodes();
            $db_links_doodstream_vf = [];
            $db_links_doodstream_vostfr = [];
            $links = [];
            $ids = [];
            $i_vf = 0;
            $i_vostfr = 0;
            $folder_saison_ids = [
                "S01" => "277599",
                "S02" => "277600",
                "S03" => "277601",
                "S04" => "277602",
                "S05" => "277604"];
            $folder_id = "";
            $links_doodstream_vf2 = [];
            $links_doodstream_vostfr2 = [];

            if ($saison->getId() >= 10){
                $model = "#S" . $saison->getId() . "#";
                $folder_id = $folder_saison_ids["S" . $saison->getId()];
            }
            if ($saison->getId() < 10){
                $model = "#S0" . $saison->getId() . "#";
                $folder_id = $folder_saison_ids["S0" . $saison->getId()];
            }

            foreach ($episodes_collection as $episode)
            {
                $db_links_doodstream_vf[$episode->getId()] = $episode->getOpenloadLinkVf();
                $db_links_doodstream_vostfr[$episode->getId()] = $episode->getOpenloadLinkVostfr();
                $ids[] = $episode->getId();

            }
            if (count($db_links_doodstream_vostfr) > 0 && count($db_links_doodstream_vf) > 0)
            {
                ksort($db_links_doodstream_vf);
                ksort($db_links_doodstream_vostfr);
            }

            $curl = new Curl();
            $fileInfo = $curl->get('https://doodapi.com/api/folder/list',[
                'key' => '58491bwdu8zfhje094kan',
                'fld_id' => $folder_id
            ]);

            $files = json_decode($fileInfo->getResponse(), true);
            $array_files = $files["result"]["files"];


            if(is_array($array_files) && count($array_files) > 0)
            {
                foreach ($array_files as $key => $value)
                {
                    $links[$value["title"]] = "https://dood.to/e/" . $value["file_code"];
                }
                ksort($links);
                //return dump($links);
                if (count($links) > 0)
                {
                    foreach ($links as $key => $value)
                    {
                        if (preg_match("#VOSTFR#", $key))
                        {
                            $links_doodstream_vostfr2[$ids[$i_vostfr++]] = $value;
                        }
                        if (preg_match("#VF#", $key))
                        {
                            $links_doodstream_vf2[$ids[$i_vf++]] = $value;
                        }
                    }
                    if (count($links_doodstream_vf2) > 0 && count($links_doodstream_vostfr2) > 0)
                    {
                        if($db_links_doodstream_vf !== $links_doodstream_vf2 && $db_links_doodstream_vostfr !== $links_doodstream_vostfr2)
                        {

                            foreach ($links_doodstream_vostfr2 as $key => $value)
                            {
                                $episode = $this->episodeRepository->find($key);
                                $episode->setOpenloadLinkVostfr($value);
                                $this->em->persist($episode);
                            }
                            foreach ($links_doodstream_vf2 as $key => $value)
                            {
                                $episode = $this->episodeRepository->find($key);
                                $episode->setOpenloadLinkVf($value);
                                $this->em->persist($episode);
                            }

                            $this->em->flush();
                            $this->addFlash('success', 'Les liens DOODSTREAM pour la ' . strtoupper($saison->getName()). ' ont été edités!');
                            return $this->redirectToRoute("admin.saisons.index");
                        }
                        else{
                            $this->addFlash('warning', 'Les liens DOODSTREAM pour la ' . strtoupper($saison->getName()). ' sont déjà à jour!');
                            return $this->redirectToRoute("admin.saisons.index");
                        }
                    }
                }
            }
            else{
                $this->addFlash('error', 'Les liens DOODSTREAM pour la ' . strtoupper($saison->getName()) . ' ont pas été edités! (too many request vers API 3max each 2-3mins)');
                return $this->redirectToRoute("admin.saisons.index");
            }
        }
        return $this->redirectToRoute('home');
    }

    /**
     * @Route("/admin/edit-linkStream-saison/{id}", name="admin.link.saison.edit.stream", methods="GET|POST")
     * @param Saison $saison
     * @param Curl $curl
     * @return \Symfony\Component\HttpFoundation\Response
     * @internal param Request $request
     */
    public function editlinkStream(Saison $saison)
    {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY'))
        {
            $episodes_collection = $saison->getEpisodes();
            $db_links_openload_vf = [];
            $db_links_openload_vostfr = [];
            $links_openload_vf = "";
            $links_openload_vf2 = [];
            $links_openload_vostfr = "";
            $links_openload_vostfr2 = [];
            $links_to_update = [];
            $links = [];
            $model = "";
            $ids = [];
            $i_vf = 0;
            $i_vostfr = 0;
            $folder_saison_ids = [
                "S01" => "131474",
                "S02" => "131475",
                "S03" => "131476",
                "S04" => "131477",
                "S05" => "131478",
                "S06" => "131479",
                "S07" => "131480",
                "S08" => "131481",];
            $folder_id = "";

            if ($saison->getId() >= 10){
                $model = "#S" . $saison->getId() . "#";
                $folder_id = $folder_saison_ids["S" . $saison->getId()];
            }
            if ($saison->getId() < 10){
                $model = "#S0" . $saison->getId() . "#";
                $folder_id = $folder_saison_ids["S0" . $saison->getId()];
            }

            foreach ($episodes_collection as $episode)
            {
                $db_links_openload_vf[$episode->getId()] = $episode->getOpenloadLinkVf();
                $db_links_openload_vostfr[$episode->getId()] = $episode->getOpenloadLinkVostfr();
                $ids[] = $episode->getId();

            }
            if (count($db_links_openload_vostfr) > 0 && count($db_links_openload_vf) > 0)
            {
                ksort($db_links_openload_vf);
                ksort($db_links_openload_vostfr);
            }


            $curl = new Curl();

            $fileInfo = $curl->get('https://api.vidoza.net/v1/files',[
                'api_token' => 'ePCDpbovRjOQI5D7viIklhD2qzmwQWEf4lSyIfafw1NS49SCwIkN73ZZIaRB',
                'folder_id' => $folder_id,
                'orderColumn' => 'name',
            ]);

            $files = json_decode($fileInfo->getResponse(), true);
            $array_files = $files["data"];

            if(is_array($array_files) && count($array_files) > 0)
            {
                foreach ($array_files as $key => $value)
                {
                    //array_push($links, $value["link"]);
                    //$links[$value["name"]] = $value["link"];
                    $completeLink = "https://vidoza.net/embed-" . $value["id"] . ".html" . "/" . $value["name"];
                    array_push($links, $completeLink);
                }

                //ksort($links);
                if (count($links) > 0)
                {
                    foreach ($links as $key => $value)
                    {
                        if (preg_match($model, $value))
                        {
                            array_push($links_to_update, $value);
                        }
                    }
                    if (count($links_to_update) > 0)
                    {
                        foreach ($links_to_update as $key => $value)
                        {
                            if (preg_match("#VOSTFR#", $value))
                            {
                                $links_openload_vostfr = explode("/", $value);
                                $links_openload_vostfr = array_slice($links_openload_vostfr, 0, 4);
                                $links_openload_vostfr = implode("/", $links_openload_vostfr);
                                $links_openload_vostfr2[$ids[$i_vostfr++]] = $links_openload_vostfr;
                            }
                            if (preg_match("#VF#", $value))
                            {
                                $links_openload_vf = explode("/", $value);
                                $links_openload_vf = array_slice($links_openload_vf, 0, 4);
                                $links_openload_vf = implode("/", $links_openload_vf);
                                $links_openload_vf2[$ids[$i_vf++]] = $links_openload_vf;
                            }
                        }
                        if (count($links_openload_vf2) > 0 && count($links_openload_vostfr2) > 0)
                        {
                            if($db_links_openload_vf !== $links_openload_vf2 && $db_links_openload_vostfr !== $links_openload_vostfr2)
                            {

                                foreach ($links_openload_vostfr2 as $key => $value)
                                {
                                    $episode = $this->episodeRepository->find($key);
                                    $episode->setStreamangoLinkVostfr($value);
                                    $this->em->persist($episode);
                                }
                                foreach ($links_openload_vf2 as $key => $value)
                                {
                                    $episode = $this->episodeRepository->find($key);
                                    $episode->setStreamangoLinkVf($value);
                                    $this->em->persist($episode);
                                }

                                $this->em->flush();
                                $this->addFlash('success', 'Les liens VIDOZA pour la ' . strtoupper($saison->getName()). ' ont été edités!');
                                return $this->redirectToRoute("admin.saisons.index");
                            }
                            else{
                                $this->addFlash('warning', 'Les liens VIDOZA pour la ' . strtoupper($saison->getName()). ' sont déjà à jour!');
                                return $this->redirectToRoute("admin.saisons.index");
                            }
                        }
                    }
                }
            }
            else{
                $this->addFlash('error', 'Les liens VIDOZA pour la ' . strtoupper($saison->getName()) . ' ont pas été edités! (too many request vers API 3max each 2-3mins)');
                return $this->redirectToRoute("admin.saisons.index");
            }
        }
        return $this->redirectToRoute('home');
    }

    /**
     * @Route("/admin/delete-saison/{id}", name="admin.saison.delete", methods="DELETE")
     * @param Saison $saison
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function delete(Saison $saison, Request $request)
    {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY'))
        {
            if($this->isCsrfTokenValid("delete" . $saison->getId(), $request->get("_token")))
            {
                $this->em->remove($saison);
                $this->em->flush();
                $this->addFlash('success', 'La saison a été suprimée!');
            }
            return $this->redirectToRoute("admin.saisons.index");
        }
        return $this->redirectToRoute('home');

    }

    /**
     * @return string
     */
    private function generateUniqueFileName()
    {
        // md5() reduces the similarity of the file names generated by
        // uniqid(), which is based on timestamps
        return md5(uniqid());
    }
}
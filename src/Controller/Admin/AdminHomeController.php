<?php
namespace App\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class AdminHomeController extends AbstractController
{

    /**
     * @Route("/admin", name="admin.home.index")
     */
    public function index()
    {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY'))
        {
            return $this->render('admin/home/index.html.twig');
        }
        return $this->redirectToRoute('home');
    }
}
<?php

namespace App\Controller;

use App\Entity\Episode;
use App\Entity\Saison;
use App\Repository\EpisodeRepository;
use App\Repository\SaisonRepository;
use Doctrine\Common\Persistence\ObjectManager;
use http\Env\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;
use Knp\Component\Pager\PaginatorInterface;

class EpisodesController extends AbstractController
{

    /**
     * @var SaisonRepository
     */
    private $repository;
    /**
     * @var ObjectManager
     */
    private $em;

    public function __construct(SaisonRepository $repository, ObjectManager $em)
    {

        $this->repository = $repository;
        $this->em = $em;
    }

    /**
     * @Route("/episodes", name="episodes")
     */
    public function index()
    {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $this->render('episodes/index.html.twig', [
                'controller_name' => 'EpisodesController',
            ]);
        }
    }


    /**
     * @Route("/{slug_s}-{id_s}/{slug}-{number}/{id}", name="episode.show", requirements={"slug": "[a-z0-9\-]*", "slug_s": "[a-z0-9\-]*"})
     * @param Episode $episode
     * @param $slug
     * @param Breadcrumbs $breadcrumbs
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showEpisode(EpisodeRepository $repository, Episode $episode, $slug, Breadcrumbs $breadcrumbs)
    {
        //$episode = $request->get()
        //dump($this->repository->find($id_s));
        //$saison = $this->repository->find($id_s);
        //$episodes = $saison->getEpisodes();
        //$episode = $episodes->toArray();
        //$episode = $episode[$number - 1];
        //dump($episode);
        //$episode = $this->repository->findBy(["title_slug_url" => $title]);

        //$episode = $this->em->find(Episode::class, $request->get('title'));
        $saison = $episode->getSaison();
        //dump($request);

        //$breadcrumbs->addItem("Home", $this->get("router")->generate("home"));

        $breadcrumbs->addItem(" " . $saison->getName(), $this->get("router")->generate("saison.show", [
            'id' => $saison->getId(),
            'slug' => $saison->getSlug()
        ]));

        $breadcrumbs->addItem(" Episode " . $episode->getNumber() . ": " . $episode->getTitle(), $this->get("router")->generate("episode.show", [
            'id' => $episode->getId(),
            'slug' => $episode->getSlug(),
            'number' => $episode->getNumber(),
            'id_s' => $saison->getId(),
            'slug_s' => $saison->getSlug(),
        ]));

        if($episode->getSlug() !== $slug)
        {
            return $this->redirectToRoute("episode.show", [
                'id' => $episode->getId(),
                'slug' => $episode->getSlug(),
                'number' => $episode->getNumber(),
                'id_s' => $saison->getId(),
                'slug_s' => $saison->getSlug(),
            ], 301);
        }
        return $this->render('episodes/show.html.twig', [
            'episode' => $episode,
            'current_menu' => $saison->getName(),
        ]);
    }
}

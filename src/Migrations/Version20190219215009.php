<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190219215009 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE episode ADD openload_link_vf LONGTEXT NOT NULL, ADD upvid_link_vf LONGTEXT NOT NULL, ADD openload_link_vostfr LONGTEXT NOT NULL, ADD upvid_link_vostfr LONGTEXT NOT NULL, DROP version, DROP openload_link, DROP upvid_link');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE episode ADD version VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, ADD openload_link LONGTEXT NOT NULL COLLATE utf8mb4_unicode_ci, ADD upvid_link LONGTEXT NOT NULL COLLATE utf8mb4_unicode_ci, DROP openload_link_vf, DROP upvid_link_vf, DROP openload_link_vostfr, DROP upvid_link_vostfr');
    }
}

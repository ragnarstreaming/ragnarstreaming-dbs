<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190221092500 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE episode ADD streamango_link_vf VARCHAR(255) NOT NULL, ADD streamango_link_vostfr VARCHAR(255) NOT NULL, DROP upvid_link_vf, DROP upvid_link_vostfr');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE episode ADD upvid_link_vf VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, ADD upvid_link_vostfr VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, DROP streamango_link_vf, DROP streamango_link_vostfr');
    }
}

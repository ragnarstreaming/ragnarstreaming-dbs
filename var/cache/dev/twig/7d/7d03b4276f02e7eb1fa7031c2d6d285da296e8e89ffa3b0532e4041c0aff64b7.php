<?php

/* saisons/index.html.twig */
class __TwigTemplate_9e74368d5bae68664c13bd8d2adc63bb77370aedc608d966010f78e47adc8e0d extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "saisons/index.html.twig", 1);
        $this->blocks = [
            'title' => [$this, 'block_title'],
            'meta' => [$this, 'block_meta'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "saisons/index.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "saisons/index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_title($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Game of Thrones Streaming Gratuit en VF et VOSTFR";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 3
    public function block_meta($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "meta"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "meta"));

        echo "Regardez toutes les saisons et tous les épisodes de la série Game of thrones en streaming gratuit VF et VOSTFR !";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 4
    public function block_body($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 5
        echo "
    <div class=\"container-fluid mt-container-home\">
        <h1 class=\"text-center mb-5\">Game of Thrones en streaming gratuit VF et VOSTFR</h1>
        <div class=\"row flex\">
                <div class=\"card-deck deck-home\">
                    ";
        // line 10
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["saisons"]) || array_key_exists("saisons", $context) ? $context["saisons"] : (function () { throw new Twig_Error_Runtime('Variable "saisons" does not exist.', 10, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["saison"]) {
            // line 11
            echo "                    <div class=\"card hvr-grow-shadow card-saison\">
                            <div class=\"ribbon\"><span>";
            // line 12
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["saison"], "name", []), "html", null, true);
            echo "</span></div>
                        <img class=\"card-img img-home\" src=\"";
            // line 13
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("uploads/saisons_directory/" . twig_get_attribute($this->env, $this->source, $context["saison"], "avatar", []))), "html", null, true);
            echo "\" alt=\"Affiche de la ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["saison"], "name", []), "html", null, true);
            echo " de game of thrones\" title=\"Affiche de la ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["saison"], "name", []), "html", null, true);
            echo " de game of thrones\">
                        <a href=\"";
            // line 14
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("saison.show", ["slug" => twig_get_attribute($this->env, $this->source, $context["saison"], "slug", []), "id" => twig_get_attribute($this->env, $this->source, $context["saison"], "id", [])]), "html", null, true);
            echo "\" class=\"card-img-overlay hvr-fade\" title=\"Regarder la ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["saison"], "name", []), "html", null, true);
            echo " de Game Of Thrones en streaming gratuit VF et VOSTFR\">
                            <div class=\"card-title card-titlehome\">
                                <h2>Regarder la ";
            // line 16
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["saison"], "name", []), "html", null, true);
            echo " de Game Of Thrones</h2>
                                <p>";
            // line 17
            echo twig_escape_filter($this->env, twig_slice($this->env, twig_get_attribute($this->env, $this->source, $context["saison"], "description", []), 0, 249), "html", null, true);
            echo "...</p>
                            </div>
                        </a>
                    </div>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['saison'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 22
        echo "                </div>
        </div>
    </div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "saisons/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  139 => 22,  128 => 17,  124 => 16,  117 => 14,  109 => 13,  105 => 12,  102 => 11,  98 => 10,  91 => 5,  82 => 4,  64 => 3,  46 => 2,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}
{% block title %}Game of Thrones Streaming Gratuit en VF et VOSTFR{% endblock %}
{% block meta %}Regardez toutes les saisons et tous les épisodes de la série Game of thrones en streaming gratuit VF et VOSTFR !{% endblock %}
{% block body %}

    <div class=\"container-fluid mt-container-home\">
        <h1 class=\"text-center mb-5\">Game of Thrones en streaming gratuit VF et VOSTFR</h1>
        <div class=\"row flex\">
                <div class=\"card-deck deck-home\">
                    {% for saison in saisons %}
                    <div class=\"card hvr-grow-shadow card-saison\">
                            <div class=\"ribbon\"><span>{{ saison.name }}</span></div>
                        <img class=\"card-img img-home\" src=\"{{ asset('uploads/saisons_directory/' ~ saison.avatar) }}\" alt=\"Affiche de la {{ saison.name }} de game of thrones\" title=\"Affiche de la {{ saison.name }} de game of thrones\">
                        <a href=\"{{ path('saison.show', {slug: saison.slug, id: saison.id}) }}\" class=\"card-img-overlay hvr-fade\" title=\"Regarder la {{ saison.name }} de Game Of Thrones en streaming gratuit VF et VOSTFR\">
                            <div class=\"card-title card-titlehome\">
                                <h2>Regarder la {{ saison.name }} de Game Of Thrones</h2>
                                <p>{{ saison.description|slice(0,249) }}...</p>
                            </div>
                        </a>
                    </div>
                    {% endfor %}
                </div>
        </div>
    </div>
{% endblock %}
", "saisons/index.html.twig", "C:\\Users\\etien\\OneDrive\\Documents\\SITES\\base\\ragnarstreaming-vampire\\templates\\saisons\\index.html.twig");
    }
}

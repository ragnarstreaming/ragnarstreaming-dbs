<?php

/* TwigBundle:Exception:exception.rdf.twig */
class __TwigTemplate_781fddff6211439c30afb0cd42defa4199d719e5c6f3d21f3b05c6b94dabe0e4 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo twig_include($this->env, $context, "@Twig/Exception/exception.xml.twig", ["exception" => ($context["exception"] ?? null)]);
        echo "
";
    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.rdf.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "TwigBundle:Exception:exception.rdf.twig", "C:\\SITES\\ragnarstreaming-dbs\\vendor\\symfony\\twig-bundle/Resources/views/Exception/exception.rdf.twig");
    }
}

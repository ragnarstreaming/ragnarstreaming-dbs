<?php

/* saisons/index.html.twig */
class __TwigTemplate_f568b4016f07873084737aee462cd5a57eaac4c5953108dcc3ba373ce7d4a490 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "saisons/index.html.twig", 1);
        $this->blocks = [
            'title' => [$this, 'block_title'],
            'meta' => [$this, 'block_meta'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = [])
    {
        echo "Dragon Ball Super Streaming Gratuit en VF et VOSTFR";
    }

    // line 3
    public function block_meta($context, array $blocks = [])
    {
        echo "Regardez toutes les saisons et tous les épisodes de la série Dragon Ball Super en streaming gratuit VF et VOSTFR !";
    }

    // line 4
    public function block_body($context, array $blocks = [])
    {
        // line 5
        echo "
    <div class=\"container-fluid mt-container-home\">
        <h1 class=\"text-center mb-5\">Dragon Ball Super en streaming gratuit VF et VOSTFR</h1>
        <div class=\"row flex\">
                <div class=\"card-deck deck-home\">
                    ";
        // line 10
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["saisons"] ?? null));
        $context['loop'] = [
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        ];
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["saison"]) {
            // line 11
            echo "                    <div class=\"card hvr-grow-shadow card-saison saison-last-";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["loop"], "last", []), "html", null, true);
            echo "\">
                            <div class=\"ribbon\"><span>";
            // line 12
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["saison"], "name", []), "html", null, true);
            echo "</span></div>
                        <img class=\"card-img img-home\" src=\"";
            // line 13
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("uploads/saisons_directory/" . twig_get_attribute($this->env, $this->source, $context["saison"], "avatar", []))), "html", null, true);
            echo "\" alt=\"Affiche de la ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["saison"], "name", []), "html", null, true);
            echo " de Dragon Ball Super\" title=\"Affiche de la ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["saison"], "name", []), "html", null, true);
            echo " de Dragon Ball Super\">
                        <a href=\"";
            // line 14
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("saison.show", ["slug" => twig_get_attribute($this->env, $this->source, $context["saison"], "slug", []), "id" => twig_get_attribute($this->env, $this->source, $context["saison"], "id", [])]), "html", null, true);
            echo "\" class=\"card-img-overlay hvr-fade\" title=\"Regarder la ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["saison"], "name", []), "html", null, true);
            echo " de Dragon Ball Super en streaming gratuit VF et VOSTFR\">
                            <div class=\"card-title card-titlehome\">
                                <h2>Regarder la ";
            // line 16
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["saison"], "name", []), "html", null, true);
            echo " de Dragon Ball Super</h2>
                                <p>";
            // line 17
            echo twig_escape_filter($this->env, twig_slice($this->env, twig_get_attribute($this->env, $this->source, $context["saison"], "description", []), 0, 249), "html", null, true);
            echo "...</p>
                            </div>
                        </a>
                    </div>
                    ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['saison'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 22
        echo "                </div>
        </div>
    </div>
";
    }

    public function getTemplateName()
    {
        return "saisons/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  120 => 22,  101 => 17,  97 => 16,  90 => 14,  82 => 13,  78 => 12,  73 => 11,  56 => 10,  49 => 5,  46 => 4,  40 => 3,  34 => 2,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "saisons/index.html.twig", "C:\\SITES\\ragnarstreaming-dbs\\templates\\saisons\\index.html.twig");
    }
}

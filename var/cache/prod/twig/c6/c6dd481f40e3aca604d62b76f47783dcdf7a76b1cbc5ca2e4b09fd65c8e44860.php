<?php

/* admin/Home/index.html.twig */
class __TwigTemplate_82c60e97e2231a69f818cfd4adbd59a26c123a10784f48a43544a23b28e9e106 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "admin/Home/index.html.twig", 1);
        $this->blocks = [
            'title' => [$this, 'block_title'],
            'meta' => [$this, 'block_meta'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = [])
    {
        echo "Dragon Ball Super Streaming - Admin Accueil";
    }

    // line 3
    public function block_meta($context, array $blocks = [])
    {
        echo "Administration du site, page d'accueil";
    }

    // line 4
    public function block_body($context, array $blocks = [])
    {
        // line 5
        echo "
    <div class=\"container mt-4 mt-container-home\">
        <h1>Administration</h1>

        <table class=\"table table-striped\">
            <thead>
            <tr>
                <th>Saisons :</th>
                <th>Episodes :</th>
            </tr>
            </thead>
            <tbody>
                <tr>
                    <td><a href=\"";
        // line 18
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin.saisons.index");
        echo "\" class=\"btn btn-primary\">Administrer</a></td>
                    <td><a href=\"";
        // line 19
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin.episodes.index");
        echo "\" class=\"btn btn-primary\">Administrer</a></td>
                </tr>
            </tbody>
        </table>
    </div>

";
    }

    public function getTemplateName()
    {
        return "admin/Home/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  68 => 19,  64 => 18,  49 => 5,  46 => 4,  40 => 3,  34 => 2,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "admin/Home/index.html.twig", "C:\\SITES\\ragnarstreaming-dbs\\templates\\admin\\Home\\index.html.twig");
    }
}

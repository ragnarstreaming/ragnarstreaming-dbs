<?php

/* admin/episodes/_form.html.twig */
class __TwigTemplate_30dac87c7e95151f237d46c419f29177da3c56fa03578931456d34929a9c0f05 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? null), 'form_start');
        echo "
<div class=\"row\">
    <div class=\"col-md-2\">
        ";
        // line 4
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "saison", []), 'row');
        echo "
    </div>
    <div class=\"col-md-1\">
        ";
        // line 7
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "number", []), 'row');
        echo "
    </div>
    <div class=\"col-md-2\">
        ";
        // line 10
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "title", []), 'row');
        echo "
    </div>
    <div class=\"col-md-5\">
        ";
        // line 13
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "avatar", []), 'row');
        echo "
    </div>
    <div class=\"col-md-2\">
        ";
        // line 16
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "releasedAt", []), 'row');
        echo "
    </div>
</div>
<h5>VF</h5>
<div class=\"row\">
    <div class=\"col-md-12\">
        ";
        // line 22
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "openload_link_vf", []), 'label', ["label" => "Doodstream VF"]);
        echo "
        ";
        // line 23
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "openload_link_vf", []), 'widget');
        echo "
        <br>
    </div>
</div>
<div class=\"row\">
    <div class=\"col-md-12\">
        ";
        // line 29
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "streamango_link_vf", []), 'label', ["label" => "Vidoza VF"]);
        echo "
        ";
        // line 30
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "streamango_link_vf", []), 'widget');
        echo "
        <br>
    </div>
</div>
<h5>VOSTFR</h5>
<div class=\"row\">
    <div class=\"col-md-12\">
        ";
        // line 37
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "openload_link_vostfr", []), 'label', ["label" => "Doodstream VOSTFR"]);
        echo "
        ";
        // line 38
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "openload_link_vostfr", []), 'widget');
        echo "
        <br>
    </div>
</div>
<div class=\"row\">
    <div class=\"col-md-12\">
        ";
        // line 44
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "streamango_link_vostfr", []), 'label', ["label" => "Vidoza VOSTFR"]);
        echo "
        ";
        // line 45
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "streamango_link_vostfr", []), 'widget');
        echo "
        <br>
    </div>
</div>
<div class=\"row\">
    <div class=\"col-md-12\">
        ";
        // line 51
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "description", []), 'row');
        echo "
    </div>
</div>
<button class=\"btn btn-primary mb-4\">";
        // line 54
        echo twig_escape_filter($this->env, (((isset($context["button"]) || array_key_exists("button", $context))) ? (_twig_default_filter(($context["button"] ?? null), "Ajouter")) : ("Ajouter")), "html", null, true);
        echo "</button>
";
        // line 55
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? null), 'form_end');
    }

    public function getTemplateName()
    {
        return "admin/episodes/_form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  125 => 55,  121 => 54,  115 => 51,  106 => 45,  102 => 44,  93 => 38,  89 => 37,  79 => 30,  75 => 29,  66 => 23,  62 => 22,  53 => 16,  47 => 13,  41 => 10,  35 => 7,  29 => 4,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "admin/episodes/_form.html.twig", "C:\\SITES\\ragnarstreaming-dbs\\templates\\admin\\episodes\\_form.html.twig");
    }
}

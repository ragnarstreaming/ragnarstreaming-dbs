<?php

/* saisons/series.html.twig */
class __TwigTemplate_889c8169085f377a02ca28a20cabe58cb13765778348ff9cf0f7adf6a06d73f1 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "saisons/series.html.twig", 1);
        $this->blocks = [
            'title' => [$this, 'block_title'],
            'meta' => [$this, 'block_meta'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = [])
    {
        echo "Regarder d'autres séries sur Dragon Ball Super Streaming Gratuit en VF et VOSTFR";
    }

    // line 3
    public function block_meta($context, array $blocks = [])
    {
        echo "Regardez d'autres séries sur Dragon Ball Super streaming gratuit en VF et VOSTFR !";
    }

    // line 4
    public function block_body($context, array $blocks = [])
    {
        // line 5
        echo "
    <div class=\"container-fluid mt-container-home\">
        <h1 class=\"text-center mb-5\">Dragon Ball Super streaming gratuit VF et VOSTFR - Séries</h1>
        <div class=\"row flex\">
            <div class=\"card-deck deck-home\">
                <div class=\"card hvr-grow-shadow card-series\">
                    <img class=\"card-img img-home\" src=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("uploads/series_directory/dbs.jpg"), "html", null, true);
        echo "\" alt=\"Affiche de Dragon Ball Super\" title=\"Affiche de Dragon Ball Super\">
                    <a href=\"https://dbs-streaming.com\" class=\"card-img-overlay hvr-fade\" title=\"série Dragon Ball Super en streaming\">
                    </a>
                </div>
                <div class=\"card hvr-grow-shadow card-series\">
                    <img class=\"card-img img-home\" src=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("uploads/series_directory/vampire.jpg"), "html", null, true);
        echo "\" alt=\"Affiche de The Vampire Diaries\" title=\"Affiche de The Vampire Diaries\">
                    <a href=\"https://the-vampire-diaries-streaming.com\" class=\"card-img-overlay hvr-fade\" title=\"série The Vampire Diaries en streaming\">
                    </a>
                </div>
                <div class=\"card hvr-grow-shadow card-series\">
                    <img class=\"card-img img-home\" src=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("uploads/series_directory/got.jpg"), "html", null, true);
        echo "\" alt=\"Affiche de game of thrones\" title=\"Affiche de game of thrones\">
                    <a href=\"https://got-streaming-gratuit.com\" class=\"card-img-overlay hvr-fade\" title=\"série Game of Thrones en streaming\">
                    </a>
                </div>
                <div class=\"card hvr-grow-shadow card-series\">
                    <img class=\"card-img img-home\" src=\"";
        // line 26
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("uploads/series_directory/harrypotter.jpg"), "html", null, true);
        echo "\" alt=\"Affiche de Harry Potter\" title=\"Affiche de Harry Potter\">
                    <a href=\"https://harry-potter-streaming.com\" class=\"card-img-overlay hvr-fade\" title=\"Harry Potter en streaming\">
                    </a>
                </div>
                <div class=\"card hvr-grow-shadow card-series\">
                    <img class=\"card-img img-home\" src=\"";
        // line 31
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("uploads/series_directory/onepiece.jpg"), "html", null, true);
        echo "\" alt=\"Affiche de One Piece\" title=\"Affiche de One Piece\">
                    <a href=\"https://voir-one-piece-streaming.com\" class=\"card-img-overlay hvr-fade\" title=\"série One Piece en streaming\">
                    </a>
                </div>
                <div class=\"card hvr-grow-shadow card-series\">
                    <img class=\"card-img img-home\" src=\"";
        // line 36
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("uploads/series_directory/demon.jpg"), "html", null, true);
        echo "\" alt=\"Affiche de Demon Slayer\" title=\"Affiche de Demon Slayer\">
                    <a href=\"https://demon-slayer-streaming.com\" class=\"card-img-overlay hvr-fade\" title=\"série Demon Slayer en streaming\">
                    </a>
                </div>
                <div class=\"card hvr-grow-shadow card-series\">
                    <img class=\"card-img img-home\" src=\"";
        // line 41
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("uploads/series_directory/shippuden.jpg"), "html", null, true);
        echo "\" alt=\"Affiche de Naruto Shippuden\" title=\"Affiche de Naruto Shippuden\">
                    <a href=\"https://naruto-shippuden-streaming.com\" class=\"card-img-overlay hvr-fade\" title=\"série Naruto Shippuden en streaming\">
                    </a>
                </div>
                <div class=\"card hvr-grow-shadow card-series\">
                    <img class=\"card-img img-home\" src=\"";
        // line 46
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("uploads/series_directory/academia.jpg"), "html", null, true);
        echo "\" alt=\"Affiche de My Hero Academia\" title=\"Affiche de My Hero Academia\">
                    <a href=\"https://myheroacademia-streaming.com\" class=\"card-img-overlay hvr-fade\" title=\"série My Hero Academia en streaming\">
                    </a>
                </div>
                <div class=\"card hvr-grow-shadow card-series\">
                    <img class=\"card-img img-home\" src=\"";
        // line 51
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("uploads/series_directory/titans.jpg"), "html", null, true);
        echo "\" alt=\"Affiche de Attaque des Titans\" title=\"Affiche de Attaque des Titans\">
                    <a href=\"https://attaquedestitans-streaming.com\" class=\"card-img-overlay hvr-fade\" title=\"série Attaque des Titans en streaming\">
                    </a>
                </div>
                <div class=\"card hvr-grow-shadow card-series\">
                    <img class=\"card-img img-home\" src=\"";
        // line 56
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("uploads/series_directory/bleach.jpg"), "html", null, true);
        echo "\" alt=\"Affiche de Bleach\" title=\"Affiche de Bleach\">
                    <a href=\"https://bleach-streaming.com\" class=\"card-img-overlay hvr-fade\" title=\"série Bleach en streaming\">
                    </a>
                </div>
                <div class=\"card hvr-grow-shadow card-series\">
                    <img class=\"card-img img-home\" src=\"";
        // line 61
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("uploads/series_directory/vikings.jpg"), "html", null, true);
        echo "\" alt=\"Affiche de Vikings\" title=\"Affiche de the Vikings\">
                    <a href=\"https://vikings-streaming.com\" class=\"card-img-overlay hvr-fade\" title=\"série Vikings en streaming\">
                    </a>
                </div>
                <div class=\"card hvr-grow-shadow card-series\">
                    <img class=\"card-img img-home\" src=\"";
        // line 66
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("uploads/series_directory/100.jpg"), "html", null, true);
        echo "\" alt=\"Affiche de the 100\" title=\"Affiche de the 100\">
                    <a href=\"https://regarder-the-100-streaming.com\" class=\"card-img-overlay hvr-fade\" title=\"série the 100 en streaming\">
                    </a>
                </div>
                <div class=\"card hvr-grow-shadow card-series\">
                    <img class=\"card-img img-home\" src=\"";
        // line 71
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("uploads/series_directory/stranger.jpg"), "html", null, true);
        echo "\" alt=\"Affiche de sranger things\" title=\"Affiche de stranger things\">
                    <a href=\"https://strangerthings-streaming.com\" class=\"card-img-overlay hvr-fade\" title=\"série Stranger Things en streaming\">
                    </a>
                </div>
                <div class=\"card hvr-grow-shadow card-series\">
                    <img class=\"card-img img-home\" src=\"";
        // line 76
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("uploads/series_directory/witcher.jpg"), "html", null, true);
        echo "\" alt=\"Affiche de the witcher\" title=\"Affiche de the witcher\">
                    <a href=\"https://regarder-the-witcher-streaming-gratuit.com\" class=\"card-img-overlay hvr-fade\" title=\"série The Witcher en streaming\">
                    </a>
                </div>
                <div class=\"card hvr-grow-shadow card-series\">
                    <img class=\"card-img img-home\" src=\"";
        // line 81
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("uploads/series_directory/malcolm.jpg"), "html", null, true);
        echo "\" alt=\"Affiche de malcolm\" title=\"Affiche de malcolm\">
                    <a href=\"https://regarder-malcolm-streaming.com\" class=\"card-img-overlay hvr-fade\" title=\"série Malcolm en streaming\">
                    </a>
                </div>
                <div class=\"card hvr-grow-shadow card-series\">
                    <img class=\"card-img img-home\" src=\"";
        // line 86
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("uploads/series_directory/walking.jpg"), "html", null, true);
        echo "\" alt=\"Affiche de The walking dead\" title=\"Affiche de The walking dead\">
                    <a href=\"https://the-walking-dead-streaming.com\" class=\"card-img-overlay hvr-fade\" title=\"série The walking dead en streaming\">
                    </a>
                </div>
                <div class=\"card hvr-grow-shadow card-series\">
                    <img class=\"card-img img-home\" src=\"";
        // line 91
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("uploads/series_directory/anatomy.jpg"), "html", null, true);
        echo "\" alt=\"Affiche de Grey's Anatomy\" title=\"Affiche de Grey's Anatomy\">
                    <a href=\"https://greys-anatomy-streaming.com\" class=\"card-img-overlay hvr-fade\" title=\"série Grey's Anatomy en streaming\">
                    </a>
                </div>
                <div class=\"card hvr-grow-shadow card-series\">
                    <img class=\"card-img img-home\" src=\"";
        // line 96
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("uploads/series_directory/peaky.jpg"), "html", null, true);
        echo "\" alt=\"Affiche de Peaky Blinders\" title=\"Affiche de Peaky Blinders\">
                    <a href=\"https://regarder-peaky-blinders-streaming.xyz\" class=\"card-img-overlay hvr-fade\" title=\"série Peaky Blinders en streaming\">
                    </a>
                </div>
                <div class=\"card hvr-grow-shadow card-series\">
                    <img class=\"card-img img-home\" src=\"";
        // line 101
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("uploads/series_directory/american.jpg"), "html", null, true);
        echo "\" alt=\"Affiche de American Horror Story\" title=\"Affiche de American Horror Story\">
                    <a href=\"https://americanhorrorstory-streaming.com\" class=\"card-img-overlay hvr-fade\" title=\"série American Horror Story en streaming\">
                    </a>
                </div>
                <div class=\"card hvr-grow-shadow card-series\">
                    <img class=\"card-img img-home\" src=\"";
        // line 106
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("uploads/series_directory/prison.jpg"), "html", null, true);
        echo "\" alt=\"Affiche de Prison Break\" title=\"Affiche de Prison Break\">
                    <a href=\"https://regarder-prison-break-streaming.com\" class=\"card-img-overlay hvr-fade\" title=\"série Prison Break en streaming\">
                    </a>
                </div>
                <div class=\"card hvr-grow-shadow card-series\">
                    <img class=\"card-img img-home\" src=\"";
        // line 111
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("uploads/series_directory/watchmen.jpg"), "html", null, true);
        echo "\" alt=\"Affiche de Watchmen\" title=\"Affiche de Watchmen\">
                    <a href=\"https://regarder-watchmen-streaming.com\" class=\"card-img-overlay hvr-fade\" title=\"série Watchmen en streaming\">
                    </a>
                </div>
                <div class=\"card hvr-grow-shadow card-series\">
                    <img class=\"card-img img-home\" src=\"";
        // line 116
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("uploads/series_directory/materials.jpg"), "html", null, true);
        echo "\" alt=\"Affiche de His Dark Materials\" title=\"Affiche de His Dark Materials\">
                    <a href=\"https://regarder-his-dark-materials-streaming.com\" class=\"card-img-overlay hvr-fade\" title=\"série His Dark Materials en streaming\">
                    </a>
                </div>
                <div class=\"card hvr-grow-shadow card-series\">
                    <img class=\"card-img img-home\" src=\"";
        // line 121
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("uploads/series_directory/bureau.jpg"), "html", null, true);
        echo "\" alt=\"Affiche de Bureau des légendes\" title=\"Affiche de Bureau des légendes\">
                    <a href=\"https://regarder-le-bureau-des-legendes-streaming.com\" class=\"card-img-overlay hvr-fade\" title=\"série Bureau des légendes en streaming\">
                    </a>
                </div>
                <div class=\"card hvr-grow-shadow card-series\">
                    <img class=\"card-img img-home\" src=\"";
        // line 126
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("uploads/series_directory/famille.jpg"), "html", null, true);
        echo "\" alt=\"Affiche de Ma Famille D'abord\" title=\"Affiche de Ma Famille D'abord\">
                    <a href=\"https://ma-famille-dabord-streaming.com\" class=\"card-img-overlay hvr-fade\" title=\"série Ma Famille D'abord en streaming\">
                    </a>
                </div>
                <div class=\"card hvr-grow-shadow card-series\">
                    <img class=\"card-img img-home\" src=\"";
        // line 131
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("uploads/series_directory/mirror.jpg"), "html", null, true);
        echo "\" alt=\"Affiche de Black Mirror\" title=\"Affiche de Black Mirror\">
                    <a href=\"https://black-mirror-streaming.com\" class=\"card-img-overlay hvr-fade\" title=\"série Black Mirror en streaming\">
                    </a>
                </div>
            </div>
        </div>
    </div>
";
    }

    public function getTemplateName()
    {
        return "saisons/series.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  249 => 131,  241 => 126,  233 => 121,  225 => 116,  217 => 111,  209 => 106,  201 => 101,  193 => 96,  185 => 91,  177 => 86,  169 => 81,  161 => 76,  153 => 71,  145 => 66,  137 => 61,  129 => 56,  121 => 51,  113 => 46,  105 => 41,  97 => 36,  89 => 31,  81 => 26,  73 => 21,  65 => 16,  57 => 11,  49 => 5,  46 => 4,  40 => 3,  34 => 2,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "saisons/series.html.twig", "C:\\SITES\\ragnarstreaming-dbs\\templates\\saisons\\series.html.twig");
    }
}

<?php

/* security/login.html.twig */
class __TwigTemplate_ae9263510f9cf86c91811db04413c3f93321464ce1314d6bb9a02ff67a82647a extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "security/login.html.twig", 1);
        $this->blocks = [
            'title' => [$this, 'block_title'],
            'meta' => [$this, 'block_meta'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        echo "Dragon Ball Super Streaming - Connexion";
    }

    // line 4
    public function block_meta($context, array $blocks = [])
    {
        echo "Se connecter à l'administration du site";
    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        // line 6
        echo "    <div class=\"container mt-4\">
        <form method=\"post\">
            ";
        // line 8
        if (($context["error"] ?? null)) {
            // line 9
            echo "                <div class=\"alert alert-danger\">";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(twig_get_attribute($this->env, $this->source, ($context["error"] ?? null), "messageKey", []), twig_get_attribute($this->env, $this->source, ($context["error"] ?? null), "messageData", []), "security"), "html", null, true);
            echo "</div>
            ";
        }
        // line 11
        echo "
            <h1 class=\"h3 mb-3 font-weight-normal\">Connexion</h1>
            <label for=\"inputEmail\" class=\"sr-only\">Email</label>
            <input type=\"email\" value=\"";
        // line 14
        echo twig_escape_filter($this->env, ($context["last_username"] ?? null), "html", null, true);
        echo "\" name=\"email\" id=\"inputEmail\" class=\"form-control\" placeholder=\"Email\" required autofocus>
            <label for=\"inputPassword\" class=\"sr-only\">Password</label>
            <input type=\"password\" name=\"password\" id=\"inputPassword\" class=\"form-control mt-4\" placeholder=\"Password\" required>

            <input type=\"hidden\" name=\"_csrf_token\"
                   value=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderCsrfToken("authenticate"), "html", null, true);
        echo "\"
            >

            ";
        // line 32
        echo "
            <button class=\"btn btn-lg btn-primary mt-4\" type=\"submit\">
                Sign in
            </button>
        </form>
    </div>

";
    }

    public function getTemplateName()
    {
        return "security/login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  80 => 32,  74 => 19,  66 => 14,  61 => 11,  55 => 9,  53 => 8,  49 => 6,  46 => 5,  40 => 4,  34 => 3,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "security/login.html.twig", "C:\\SITES\\ragnarstreaming-dbs\\templates\\security\\login.html.twig");
    }
}

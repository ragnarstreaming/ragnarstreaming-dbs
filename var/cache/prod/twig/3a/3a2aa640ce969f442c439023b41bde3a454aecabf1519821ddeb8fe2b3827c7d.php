<?php

/* WhiteOctoberBreadcrumbsBundle::json-ld.html.twig */
class __TwigTemplate_800bcb1f1b0df75358c24297c34198f7f01ff0f4057f30c05e6b27e2205fd8b5 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        if (twig_length_filter($this->env, $this->extensions['WhiteOctober\BreadcrumbsBundle\Twig\Extension\BreadcrumbsExtension']->getBreadcrumbs())) {
            // line 2
            ob_start();
            // line 3
            echo "<script type=\"application/ld+json\">
        {
            \"@context\": \"http://schema.org\",
            \"@type\": \"BreadcrumbList\",
            \"itemListElement\":
            [
                ";
            // line 9
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["breadcrumbs"] ?? null));
            $context['loop'] = [
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            ];
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["b"]) {
                // line 10
                echo "                    {
                        \"@type\": \"ListItem\",
                        \"position\": ";
                // line 12
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["loop"], "index", []), "html", null, true);
                echo ",
                        \"item\":
                        {
                            \"@id\": \"";
                // line 15
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["b"], "url", []), "html", null, true);
                echo "\",
                            \"name\": \"";
                // line 16
                if ((twig_get_attribute($this->env, $this->source, $context["b"], "translate", [], "any", true, true) && (twig_get_attribute($this->env, $this->source, $context["b"], "translate", []) == true))) {
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(twig_get_attribute($this->env, $this->source, $context["b"], "text", []), twig_get_attribute($this->env, $this->source, $context["b"], "translationParameters", []), ($context["translation_domain"] ?? null), ($context["locale"] ?? null)), "html", null, true);
                } else {
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["b"], "text", []), "html", null, true);
                }
                echo "\"
                        }
                    }";
                // line 18
                if ( !twig_get_attribute($this->env, $this->source, $context["loop"], "last", [])) {
                    echo ",";
                }
                // line 19
                echo "                ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['b'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 20
            echo "            ]
        }
        </script>";
            echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        }
    }

    public function getTemplateName()
    {
        return "WhiteOctoberBreadcrumbsBundle::json-ld.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  93 => 20,  79 => 19,  75 => 18,  66 => 16,  62 => 15,  56 => 12,  52 => 10,  35 => 9,  27 => 3,  25 => 2,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "WhiteOctoberBreadcrumbsBundle::json-ld.html.twig", "C:\\SITES\\ragnarstreaming-dbs\\vendor\\whiteoctober\\breadcrumbs-bundle\\WhiteOctober\\BreadcrumbsBundle/Resources/views/json-ld.html.twig");
    }
}

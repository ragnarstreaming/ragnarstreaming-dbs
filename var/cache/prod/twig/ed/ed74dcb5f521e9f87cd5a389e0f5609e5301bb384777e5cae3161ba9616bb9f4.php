<?php

/* @Twig/Exception/error404.html.twig */
class __TwigTemplate_52f0e38f1cd8283e3d1e82b7304399445b286ee7fe285328b8207934e10d29f4 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "@Twig/Exception/error404.html.twig", 1);
        $this->blocks = [
            'title' => [$this, 'block_title'],
            'meta' => [$this, 'block_meta'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = [])
    {
        echo "Dragon Ball Super Streaming - Pas introuvable";
    }

    // line 3
    public function block_meta($context, array $blocks = [])
    {
        echo "Pas d'erreur 404 pour le site Dragon Ball Super en streaming";
    }

    // line 4
    public function block_body($context, array $blocks = [])
    {
        // line 5
        echo "    <div class=\"container mt-container-home\">
        <h1 class=\"text-center font-weight-bold title-404 mt-4 mb-1\" title=\"ERREUR 404\">ERREUR 404</h1>
    </div>
";
    }

    public function getTemplateName()
    {
        return "@Twig/Exception/error404.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 5,  46 => 4,  40 => 3,  34 => 2,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@Twig/Exception/error404.html.twig", "C:\\SITES\\ragnarstreaming-dbs\\templates\\bundles\\TwigBundle\\Exception\\error404.html.twig");
    }
}

<?php

/* base.html.twig */
class __TwigTemplate_3a5fbefcd99532ae78f6288208706faf456d24f8ad861c4e21beeda6cf9bc513 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'meta' => [$this, 'block_meta'],
            'stylesheets' => [$this, 'block_stylesheets'],
            'body' => [$this, 'block_body'],
            'javascripts' => [$this, 'block_javascripts'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <!--<script async src=\"https://www.googletagmanager.com/gtag/js?id=UA-210636927-1\"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-210636927-1');
        </script>-->

        <meta charset=\"UTF-8\">
        <title>";
        // line 15
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        <meta name=\"description\" content=\"";
        // line 16
        $this->displayBlock('meta', $context, $blocks);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/css/app.css"), "html", null, true);
        echo "\">
        <link rel=\"icon\" type=\"image/png\" href=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("uploads/logo.png"), "html", null, true);
        echo "\" />
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">

        ";
        // line 21
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 22
        echo "    </head>
    <body>
    <nav class=\"navbar fixed-top navbar-expand-lg navbar-dark bg-dark got-nav\">
        <a class=\"navbar-brand link-home\" href=\"";
        // line 25
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("home");
        echo "\" title=\"Regarder Dragon Ball Super en Streaming gratuit VF et VOSTFR\">Dragon Ball Super Streaming</a>
        <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarNav\" aria-controls=\"navbarNav\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
            <span class=\"navbar-toggler-icon\"></span>
        </button>
        <div class=\"collapse navbar-collapse\" id=\"navbarNav\">
            <ul class=\"navbar-nav\">
                <li class=\"nav-item\">
                    <a class=\"nav-link float-left ";
        // line 32
        if (((isset($context["current_menu"]) || array_key_exists("current_menu", $context)) && (($context["current_menu"] ?? null) == "Accueil"))) {
            echo " active ";
        }
        echo "\" title=\"Regarder les saisons de Dragon Ball Super en streaming VF et VOSTFR gratuit\" href=\"/\">Accueil</a>
                </li>
                <li class=\"nav-item\">
                    <a class=\"nav-link float-left ";
        // line 35
        if (((isset($context["current_menu"]) || array_key_exists("current_menu", $context)) && (($context["current_menu"] ?? null) == "Saison 1"))) {
            echo " active ";
        }
        echo "\" title=\"Regarder la saison 1 de Dragon Ball Super en streaming VF et VOSTFR gratuit\" href=\"/dbs-streaming-gratuit-saison-1\">Saison 1</a>
                </li>
                <li class=\"nav-item\">
                    <a class=\"nav-link float-left ";
        // line 38
        if (((isset($context["current_menu"]) || array_key_exists("current_menu", $context)) && (($context["current_menu"] ?? null) == "Saison 2"))) {
            echo " active ";
        }
        echo "\" title=\"Regarder la saison 2 de Dragon Ball Super en streaming VF et VOSTFR gratuit\" href=\"/dbs-streaming-gratuit-saison-2\">Saison 2</a>
                </li>
                <li class=\"nav-item\">
                    <a class=\"nav-link float-left ";
        // line 41
        if (((isset($context["current_menu"]) || array_key_exists("current_menu", $context)) && (($context["current_menu"] ?? null) == "Saison 3"))) {
            echo " active ";
        }
        echo "\" title=\"Regarder la saison 3 de Dragon Ball Super en streaming VF et VOSTFR gratuit\" href=\"/dbs-streaming-gratuit-saison-3\">Saison 3</a>
                </li>
                <li class=\"nav-item\">
                    <a class=\"nav-link float-left ";
        // line 44
        if (((isset($context["current_menu"]) || array_key_exists("current_menu", $context)) && (($context["current_menu"] ?? null) == "Saison 4"))) {
            echo " active ";
        }
        echo "\" title=\"Regarder la saison 4 de Dragon Ball Super en streaming VF et VOSTFR gratuit\" href=\"/dbs-streaming-gratuit-saison-4\">Saison 4</a>
                </li>
                <li class=\"nav-item\">
                    <a class=\"nav-link float-left ";
        // line 47
        if (((isset($context["current_menu"]) || array_key_exists("current_menu", $context)) && (($context["current_menu"] ?? null) == "Saison 5"))) {
            echo " active ";
        }
        echo "\" title=\"Regarder la saison 5 de Dragon Ball Super en streaming VF et VOSTFR gratuit\" href=\"/dbs-streaming-gratuit-saison-5\">Saison 5</a>
                </li>

                <li class=\"nav-item dropdown\">
                    <a class=\"nav-link float-left ";
        // line 51
        if (((isset($context["current_menu"]) || array_key_exists("current_menu", $context)) && (($context["current_menu"] ?? null) == "Autres series"))) {
            echo " active ";
        }
        echo "\" title=\"Regarder d'autres séries\" href=\"/series\">AUTRES SERIES</a>
                </li>
            </ul>
        </div>
    </nav>
    <!-- Go to www.addthis.com/dashboard to customize your tools -->
    <script type=\"text/javascript\" src=\"//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-618f09ed7dc2c5d3\"></script>

        ";
        // line 59
        $this->displayBlock('body', $context, $blocks);
        // line 60
        echo "     <footer class=\"my_footer\">
        <div class=\"container\">
            <h4 class=\"footer-title\">dbs-streaming.com VF ET VOSTFR gratuit</h4>
            <p>
                Ce site n'héberge aucun fichier vidéo. Nous ne faisons que répertorier du contenu se situant sur divers
                hébergeurs légalement reconnus... Si vous constatez quoi que ce soit, veuillez prendre contact avec l'hébergeur en question.
            </p>
        </div>
    </footer>
        ";
        // line 69
        $this->displayBlock('javascripts', $context, $blocks);
        // line 70
        echo "        <script src=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/runtime.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 71
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/js/app.js"), "html", null, true);
        echo "\"></script>


    </body>


</html>
";
    }

    // line 15
    public function block_title($context, array $blocks = [])
    {
        echo "Welcome!";
    }

    // line 16
    public function block_meta($context, array $blocks = [])
    {
        echo "Regarder toutes les saisons et tous les épisodes de la série Dragon Ball Super en streaming gratuit!";
    }

    // line 21
    public function block_stylesheets($context, array $blocks = [])
    {
    }

    // line 59
    public function block_body($context, array $blocks = [])
    {
    }

    // line 69
    public function block_javascripts($context, array $blocks = [])
    {
    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  195 => 69,  190 => 59,  185 => 21,  179 => 16,  173 => 15,  161 => 71,  156 => 70,  154 => 69,  143 => 60,  141 => 59,  128 => 51,  119 => 47,  111 => 44,  103 => 41,  95 => 38,  87 => 35,  79 => 32,  69 => 25,  64 => 22,  62 => 21,  56 => 18,  52 => 17,  48 => 16,  44 => 15,  28 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "base.html.twig", "C:\\SITES\\ragnarstreaming-dbs\\templates\\base.html.twig");
    }
}

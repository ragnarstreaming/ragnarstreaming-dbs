<?php

/* episodes/show.html.twig */
class __TwigTemplate_118c2de79d718e5f92210722ae2920db3f34813b4aebf5efa17456f836b27c05 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "episodes/show.html.twig", 1);
        $this->blocks = [
            'title' => [$this, 'block_title'],
            'meta' => [$this, 'block_meta'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = [])
    {
        echo "Dragon Ball Super Streaming - ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "saison", []), "html", null, true);
        echo " / Episode ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "number", []), "html", null, true);
        echo " - ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "title", []), "html", null, true);
        echo " en VF et VOSTFR";
    }

    // line 3
    public function block_meta($context, array $blocks = [])
    {
        echo "Regarder l'épisode ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "number", []), "html", null, true);
        echo " de la ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "saison", []), "html", null, true);
        echo " de la série Dragon Ball Super en streaming gratuit VF et VOSTFR !";
    }

    // line 4
    public function block_body($context, array $blocks = [])
    {
        // line 5
        echo "
    <div class=\"container mt-container-home\">
        <h1 class=\"mb-3 mt-3\">";
        // line 7
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "saison", []), "html", null, true);
        echo " / Episode ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "number", []), "html", null, true);
        echo " : ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "title", []), "html", null, true);
        echo "</h1>
        <h2 class=\"mb-4 reform-title\">Regarder l'épisode ";
        // line 8
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "number", []), "html", null, true);
        echo " de la ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "saison", []), "html", null, true);
        echo " de Dragon Ball Super en streaming gratuit VF et VOSTFR</h2>
        <div class=\"row mb-4\">
            <img src=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("uploads/episodes_directory/" . twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "avatar", []))), "html", null, true);
        echo "\" alt=\"Affiche de ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "title", []), "html", null, true);
        echo " de Dragon Ball Super\" class=\"col-2 avatar-img-show\" title=\"Affiche de ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "title", []), "html", null, true);
        echo " de Dragon Ball Super\">
            <p class=\"col-md-10\">";
        // line 11
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "description", []), "html", null, true);
        echo " <br><br> <span class=\"font-italic\">Première diffusion: ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "releasedAt", []), "html", null, true);
        echo "</span></p>
            <div class=\"addthis-episode-show addthis_inline_share_toolbox_35i7\"></div>
        </div>
        <section id=\"tabs\">
            <div class=\"container\">
                <div class=\"row\">
                    ";
        // line 17
        echo $this->extensions['WhiteOctober\BreadcrumbsBundle\Twig\Extension\BreadcrumbsExtension']->renderBreadcrumbs();
        echo "
                </div>


                <div class=\"row\">
                    <div class=\"col-xs-12 tab-lecteur\">
                        <nav>
                            <div class=\"nav nav-tabs nav-fill\" id=\"nav-tab\" role=\"tablist\">
                                <a class=\"nav-item nav-link active\" id=\"nav-mixdropvf-tab\" title=\"Regarder l'Episode ";
        // line 25
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "number", []), "html", null, true);
        echo " de la ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "saison", []), "html", null, true);
        echo " de Dragon Ball Super en streaming VF gratuit\" data-toggle=\"tab\" href=\"#nav-mixdropvf\" role=\"tab\" aria-controls=\"nav-mixdropvf\" aria-selected=\"true\">
                                    Doodstream
                                    <img src=\"";
        // line 27
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("uploads/logos_directory/vf.png"), "html", null, true);
        echo "\" alt=\"Logo version VF pour episode ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "title", []), "html", null, true);
        echo " de Dragon Ball Super\" class=\"logo-version\">
                                </a>
                                <a class=\"nav-item nav-link\" id=\"nav-mixdropvostfr-tab\" data-toggle=\"tab\" href=\"#nav-mixdropvostfr\" role=\"tab\" aria-controls=\"nav-mixdropvostfr\" aria-selected=\"false\" title=\"Regarder l'Episode ";
        // line 29
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "number", []), "html", null, true);
        echo " de la ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "saison", []), "html", null, true);
        echo " de Dragon Ball Super en streaming VOSTFR gratuit\">
                                    Doodstream
                                    <img src=\"";
        // line 31
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("uploads/logos_directory/vostfr.png"), "html", null, true);
        echo "\" alt=\"Logo version VOSTFR pour episode ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "title", []), "html", null, true);
        echo " de Dragon Ball Super\" class=\"logo-version\">
                                </a>
                                <a class=\"nav-item nav-link\" id=\"nav-vidozavf-tab\" data-toggle=\"tab\" href=\"#nav-vidozavf\" role=\"tab\" aria-controls=\"nav-vidozavf\" aria-selected=\"false\" title=\"Regarder l'Episode ";
        // line 33
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "number", []), "html", null, true);
        echo " de la ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "saison", []), "html", null, true);
        echo " de Dragon Ball Super en streaming VF gratuit\">
                                    Vidoza
                                    <img src=\"";
        // line 35
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("uploads/logos_directory/vf.png"), "html", null, true);
        echo "\" alt=\"Logo version VF pour episode ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "title", []), "html", null, true);
        echo " de Dragon Ball Super\" class=\"logo-version\">
                                </a>
                                <a class=\"nav-item nav-link\" id=\"nav-vidozavostfr-tab\" data-toggle=\"tab\" href=\"#nav-vidozavostfr\" role=\"tab\" aria-controls=\"nav-vidozavostfr\" aria-selected=\"false\" title=\"Regarder l'Episode ";
        // line 37
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "number", []), "html", null, true);
        echo " de la ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "saison", []), "html", null, true);
        echo " de Dragon Ball Super en streaming VOSTFR gratuit\">
                                    Vidoza
                                    <img src=\"";
        // line 39
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("uploads/logos_directory/vostfr.png"), "html", null, true);
        echo "\" alt=\"Logo version VOSTFR pour episode ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "title", []), "html", null, true);
        echo " de Dragon Ball Super\" class=\"logo-version\">
                                </a>
                            </div>
                        </nav>
                        <div class=\"tab-content py-3 px-3 px-sm-0\" id=\"nav-tabContent\">
                            <!--<script src=\"//pubdirecte.com/script/banniere.php?said=131935\"></script>
                            <button class=\"btn btn-secondary btn-show-episode\" id=\"close_pubdirecte\">FERMER PUB</button>-->
                            <div class=\"tab-pane fade show active\" id=\"nav-mixdropvf\" role=\"tabpanel\" aria-labelledby=\"nav-mixdropvf-tab\">
                                <div class=\"tab-content\">
                                    ";
        // line 48
        if ((twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "OpenloadLinkVf", [])) > 24)) {
            // line 49
            echo "                                        <iframe src=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "OpenloadLinkVf", []), "html", null, true);
            echo "\" scrolling=\"no\" frameborder=\"0\" allowfullscreen=\"true\" webkitallowfullscreen=\"true\" mozallowfullscreen=\"true\" class=\"lecteur\"></iframe>
                                    ";
        }
        // line 51
        echo "                                    ";
        if ((twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "OpenloadLinkVf", [])) < 24)) {
            // line 52
            echo "                                        <p class=\"text-center mt-3 mb-3\">La version Française n'est pas encore disponible !</p>
                                    ";
        }
        // line 54
        echo "                                </div>
                            </div>
                            <div class=\"tab-pane fade\" id=\"nav-vidozavf\" role=\"tabpanel\" aria-labelledby=\"nav-vidozavf-tab\">
                                <div class=\"tab-content\">
                                    ";
        // line 58
        if ((twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "StreamangoLinkVf", [])) > 24)) {
            // line 59
            echo "                                        <iframe src=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "StreamangoLinkVf", []), "html", null, true);
            echo "\" scrolling=\"no\" frameborder=\"0\" allowfullscreen=\"true\" webkitallowfullscreen=\"true\" mozallowfullscreen=\"true\" class=\"lecteur\"></iframe>
                                    ";
        }
        // line 61
        echo "                                    ";
        if ((twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "StreamangoLinkVf", [])) < 24)) {
            // line 62
            echo "                                        <p class=\"text-center mt-3 mb-3\">La version Française n'est pas encore disponible !</p>
                                    ";
        }
        // line 64
        echo "                                </div>
                            </div>
                            <div class=\"tab-pane fade\" id=\"nav-mixdropvostfr\" role=\"tabpanel\" aria-labelledby=\"nav-mixdropvostfr-tab\">
                                <div class=\"tab-content\">
                                    ";
        // line 68
        if ((twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "OpenloadLinkVostfr", [])) > 24)) {
            // line 69
            echo "                                        <iframe src=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "OpenloadLinkVostfr", []), "html", null, true);
            echo "\" scrolling=\"no\" frameborder=\"0\" allowfullscreen=\"true\" webkitallowfullscreen=\"true\" mozallowfullscreen=\"true\" class=\"lecteur\"></iframe>
                                    ";
        }
        // line 71
        echo "                                    ";
        if ((twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "OpenloadLinkVostfr", [])) < 24)) {
            // line 72
            echo "                                        <p class=\"text-center mt-3 mb-3\">La version originale avec les sous titres en Français n'est pas encore disponible !</p>
                                    ";
        }
        // line 74
        echo "                                </div>
                            </div>
                            <div class=\"tab-pane fade\" id=\"nav-vidozavostfr\" role=\"tabpanel\" aria-labelledby=\"nav-vidozavostfr-tab\">
                                <div class=\"tab-content\">
                                    ";
        // line 78
        if ((twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "StreamangoLinkVostfr", [])) > 24)) {
            // line 79
            echo "                                        <iframe src=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "StreamangoLinkVostfr", []), "html", null, true);
            echo "\" scrolling=\"no\" frameborder=\"0\" allowfullscreen=\"true\" webkitallowfullscreen=\"true\" mozallowfullscreen=\"true\" class=\"lecteur\"></iframe>
                                    ";
        }
        // line 81
        echo "                                    ";
        if ((twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["episode"] ?? null), "StreamangoLinkVostfr", [])) < 24)) {
            // line 82
            echo "                                        <p class=\"text-center mt-3 mb-3\">La version originale avec les sous titres en Français n'est pas encore disponible !</p>
                                    ";
        }
        // line 84
        echo "                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </section>
    </div>

";
    }

    public function getTemplateName()
    {
        return "episodes/show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  249 => 84,  245 => 82,  242 => 81,  236 => 79,  234 => 78,  228 => 74,  224 => 72,  221 => 71,  215 => 69,  213 => 68,  207 => 64,  203 => 62,  200 => 61,  194 => 59,  192 => 58,  186 => 54,  182 => 52,  179 => 51,  173 => 49,  171 => 48,  157 => 39,  150 => 37,  143 => 35,  136 => 33,  129 => 31,  122 => 29,  115 => 27,  108 => 25,  97 => 17,  86 => 11,  78 => 10,  71 => 8,  63 => 7,  59 => 5,  56 => 4,  46 => 3,  34 => 2,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "episodes/show.html.twig", "C:\\SITES\\ragnarstreaming-dbs\\templates\\episodes\\show.html.twig");
    }
}

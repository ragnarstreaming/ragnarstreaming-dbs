<?php

/* admin/episodes/index.html.twig */
class __TwigTemplate_57f5f628556d0a902d9439033539993cd178e5dd280e1d977401182c362e434d extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "admin/episodes/index.html.twig", 1);
        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = [])
    {
        echo "Dragon Ball Super Streaming - Admin épisodes";
    }

    // line 3
    public function block_body($context, array $blocks = [])
    {
        // line 4
        echo "
    <div class=\"container mt-4 mt-container-home\">

        <div class=\"row\">
            <div class=\"col-8\">
                <h1>Gérer les épisodes</h1>
            </div>
            <div class=\"col-4\">
                <a href=\"";
        // line 12
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin.saisons.index");
        echo "\" class=\"btn btn-primary float-right\">Gérer les saisons</a>
            </div>
        </div>

        ";
        // line 16
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "flashes", [0 => "success"], "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
            // line 17
            echo "            <div class=\"alert alert-success alert-dismissible fade show\" role=\"alert\">
                ";
            // line 18
            echo twig_escape_filter($this->env, $context["message"], "html", null, true);
            echo "
            </div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 21
        echo "
        ";
        // line 22
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "flashes", [0 => "warning"], "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
            // line 23
            echo "            <div class=\"alert alert-warning alert-dismissible fade show\" role=\"alert\">
                ";
            // line 24
            echo twig_escape_filter($this->env, $context["message"], "html", null, true);
            echo "
            </div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 27
        echo "
        ";
        // line 28
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "flashes", [0 => "error"], "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
            // line 29
            echo "            <div class=\"alert alert-danger alert-dismissible fade show\" role=\"alert\">
                ";
            // line 30
            echo twig_escape_filter($this->env, $context["message"], "html", null, true);
            echo "
            </div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 33
        echo "
        <table class=\"table table-striped\">
            <thead>
            <tr>
                <th>Saison</th>
                <th>Numéro</th>
                <th>Titre :</th>
                <th>Actions :</th>
            </tr>
            </thead>
            <tbody>
            ";
        // line 44
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["episodes"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["episode"]) {
            // line 45
            echo "                <tr>
                    <td>";
            // line 46
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["episode"], "saison", []), "html", null, true);
            echo "</td>
                    <td>Episode ";
            // line 47
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["episode"], "number", []), "html", null, true);
            echo "</td>
                    <td>";
            // line 48
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["episode"], "title", []), "html", null, true);
            echo "</td>
                    <td>
                        <a href=\"";
            // line 50
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin.episode.edit", ["id" => twig_get_attribute($this->env, $this->source, $context["episode"], "id", [])]), "html", null, true);
            echo "\" class=\"btn btn-secondary\">Editer</a>
                        <a href=\"";
            // line 51
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin.link.episode.edit", ["id" => twig_get_attribute($this->env, $this->source, $context["episode"], "id", [])]), "html", null, true);
            echo "\" class=\"btn btn-secondary\">Editer links Doodstream</a>
                        <a href=\"";
            // line 52
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin.link.episode.edit.stream", ["id" => twig_get_attribute($this->env, $this->source, $context["episode"], "id", [])]), "html", null, true);
            echo "\" class=\"btn btn-secondary\">Editer links Vidoza</a>
                        <form method=\"post\" action=\"";
            // line 53
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin.episode.delete", ["id" => twig_get_attribute($this->env, $this->source, $context["episode"], "id", [])]), "html", null, true);
            echo "\" style=\"display: inline-block\" onsubmit=\"return confirm('Est-ce ton dernier mot Jean-Pierre ?')\">
                            <input type=\"hidden\" name=\"_method\" value=\"DELETE\">
                            <input type=\"hidden\" name=\"_token\" value=\"";
            // line 55
            echo twig_escape_filter($this->env, $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderCsrfToken(("delete" . twig_get_attribute($this->env, $this->source, $context["episode"], "id", []))), "html", null, true);
            echo "\">
                            <button class=\"btn btn-danger\">Supprimer</button>
                        </form>
                    </td>
                </tr>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['episode'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 61
        echo "            </tbody>
        </table>

        <div class=\"text-right\">
            <a href=\"";
        // line 65
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin.episode.create");
        echo "\" class=\"btn btn-primary mb-4\">Ajouter un épisode</a>
        </div>

    </div>

";
    }

    public function getTemplateName()
    {
        return "admin/episodes/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  181 => 65,  175 => 61,  163 => 55,  158 => 53,  154 => 52,  150 => 51,  146 => 50,  141 => 48,  137 => 47,  133 => 46,  130 => 45,  126 => 44,  113 => 33,  104 => 30,  101 => 29,  97 => 28,  94 => 27,  85 => 24,  82 => 23,  78 => 22,  75 => 21,  66 => 18,  63 => 17,  59 => 16,  52 => 12,  42 => 4,  39 => 3,  33 => 2,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "admin/episodes/index.html.twig", "C:\\SITES\\ragnarstreaming-dbs\\templates\\admin\\episodes\\index.html.twig");
    }
}

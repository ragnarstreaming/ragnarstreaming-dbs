<?php

/* admin/episodes/create.html.twig */
class __TwigTemplate_6d7199ed2eecb0be47b8b60eea0088fe9cb173e87746e5309c944955511117e4 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "admin/episodes/create.html.twig", 1);
        $this->blocks = [
            'title' => [$this, 'block_title'],
            'meta' => [$this, 'block_meta'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = [])
    {
        echo "Dragon Ball Super Streaming - Admin création";
    }

    // line 3
    public function block_meta($context, array $blocks = [])
    {
        echo "Administration et création d'épisodes du site";
    }

    // line 4
    public function block_body($context, array $blocks = [])
    {
        // line 5
        echo "
    <div class=\"container mt-4 mt-container-home\">
        <h2 class=\"mb-4\">Ajouter un épisode</h2>

        ";
        // line 9
        echo twig_include($this->env, $context, "admin/episodes/_form.html.twig");
        echo "
    </div>

";
    }

    public function getTemplateName()
    {
        return "admin/episodes/create.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  55 => 9,  49 => 5,  46 => 4,  40 => 3,  34 => 2,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "admin/episodes/create.html.twig", "C:\\SITES\\ragnarstreaming-dbs\\templates\\admin\\episodes\\create.html.twig");
    }
}

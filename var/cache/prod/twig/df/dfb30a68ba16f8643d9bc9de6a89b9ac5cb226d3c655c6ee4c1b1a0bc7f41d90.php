<?php

/* saisons/show.html.twig */
class __TwigTemplate_3ef2f7d997c07c0e4c25d559da9d7a330b6203d6784b93754aa661f8cd7932b1 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "saisons/show.html.twig", 1);
        $this->blocks = [
            'title' => [$this, 'block_title'],
            'meta' => [$this, 'block_meta'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = [])
    {
        echo "Dragon Ball Super Streaming - ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["saison"] ?? null), "name", []), "html", null, true);
        echo " en VF et VOSTFR";
    }

    // line 3
    public function block_meta($context, array $blocks = [])
    {
        echo "Regarder tous les épisodes de la ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["saison"] ?? null), "name", []), "html", null, true);
        echo " de la série Dragon Ball Super en streaming gratuit VF et VOSTFR!";
    }

    // line 4
    public function block_body($context, array $blocks = [])
    {
        // line 5
        echo "
    <div class=\"container mt-container-home\">
        <h1 class=\"mb-3 mt-3\">";
        // line 7
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["saison"] ?? null), "name", []), "html", null, true);
        echo " de Dragon Ball Super</h1>
        <h2 class=\"mb-4 reform-title\">Regarder les épisodes de la ";
        // line 8
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["saison"] ?? null), "name", []), "html", null, true);
        echo " de la série Dragon Ball Super en streaming gratuit VF et VOSTFR</h2>
        <div class=\"row mb-4\">
            <img src=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("uploads/saisons_directory/" . twig_get_attribute($this->env, $this->source, ($context["saison"] ?? null), "avatar", []))), "html", null, true);
        echo "\" alt=\"Affiche de la ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["saison"] ?? null), "name", []), "html", null, true);
        echo " de Dragon Ball Super\" title=\"Affiche de la ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["saison"] ?? null), "name", []), "html", null, true);
        echo " de Dragon Ball Super\" class=\"col-2 avatar-img-show\">
            <p class=\"col-md-10\">";
        // line 11
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["saison"] ?? null), "description", []), "html", null, true);
        echo "</p>
            <div class=\"addthis-episode-show addthis_inline_share_toolbox_35i7\"></div>
        </div>

        <div class=\"container\">
            <div class=\"card-deck deck-show\">
                ";
        // line 17
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["episodes"] ?? null));
        $context['loop'] = [
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        ];
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["episode"]) {
            // line 18
            echo "                    <div class=\"card card-show card-last-";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["loop"], "last", []), "html", null, true);
            echo "\">
                        <a href=\"";
            // line 19
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("episode.show", ["slug_s" => twig_get_attribute($this->env, $this->source, ($context["saison"] ?? null), "slug", []), "id_s" => twig_get_attribute($this->env, $this->source, ($context["saison"] ?? null), "id", []), "slug" => twig_get_attribute($this->env, $this->source, $context["episode"], "slug", []), "id" => twig_get_attribute($this->env, $this->source, $context["episode"], "id", []), "number" => twig_get_attribute($this->env, $this->source, $context["episode"], "number", [])]), "html", null, true);
            echo "\" class=\"img-show\" title=\"Regarder l'Episode ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["episode"], "number", []), "html", null, true);
            echo " de la ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["saison"] ?? null), "name", []), "html", null, true);
            echo " en streaming gratuit VF et VOSTFR\">
                            <img class=\"card-img-top\" src=\"";
            // line 20
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("uploads/episodes_directory/" . twig_get_attribute($this->env, $this->source, $context["episode"], "avatar", []))), "html", null, true);
            echo "\" alt=\"Affiche de ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["episode"], "title", []), "html", null, true);
            echo " de Dragon Ball Super\">
                        </a>
                        <div class=\"card-body body-show\">
                            <h3 class=\"card-title\">Episode ";
            // line 23
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["episode"], "number", []), "html", null, true);
            echo " - ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["episode"], "title", []), "html", null, true);
            echo " </h3>
                            <p class=\"card-text\">";
            // line 24
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["episode"], "description", []), "html", null, true);
            echo "</p>
                        </div>
                        <a href=\"";
            // line 26
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("episode.show", ["slug_s" => twig_get_attribute($this->env, $this->source, ($context["saison"] ?? null), "slug", []), "id_s" => twig_get_attribute($this->env, $this->source, ($context["saison"] ?? null), "id", []), "slug" => twig_get_attribute($this->env, $this->source, $context["episode"], "slug", []), "id" => twig_get_attribute($this->env, $this->source, $context["episode"], "id", []), "number" => twig_get_attribute($this->env, $this->source, $context["episode"], "number", [])]), "html", null, true);
            echo "\" class=\"btn btn-secondary btn-lg btn-show\" role=\"button\" title=\"Regarder l'Episode ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["episode"], "number", []), "html", null, true);
            echo " de la ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["saison"] ?? null), "name", []), "html", null, true);
            echo " en streaming gratuit VF et VOSTFR\">Regarder</a>
                    </div>
                ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['episode'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 29
        echo "            </div>
        </div>
    </div>




";
    }

    public function getTemplateName()
    {
        return "saisons/show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  153 => 29,  132 => 26,  127 => 24,  121 => 23,  113 => 20,  105 => 19,  100 => 18,  83 => 17,  74 => 11,  66 => 10,  61 => 8,  57 => 7,  53 => 5,  50 => 4,  42 => 3,  34 => 2,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "saisons/show.html.twig", "C:\\SITES\\ragnarstreaming-dbs\\templates\\saisons\\show.html.twig");
    }
}

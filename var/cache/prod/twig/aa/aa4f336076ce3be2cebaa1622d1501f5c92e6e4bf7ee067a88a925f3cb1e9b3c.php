<?php

/* @WhiteOctoberBreadcrumbs/microdata.html.twig */
class __TwigTemplate_e3240fb43293b6513ae729e061704f28cf52052614b4ee671ada6ce010c8f10d extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        if (twig_length_filter($this->env, $this->extensions['WhiteOctober\BreadcrumbsBundle\Twig\Extension\BreadcrumbsExtension']->getBreadcrumbs())) {
            // line 2
            ob_start();
            // line 3
            echo "<ol id=\"";
            echo twig_escape_filter($this->env, ($context["listId"] ?? null), "html", null, true);
            echo "\" class=\"";
            echo twig_escape_filter($this->env, ($context["listClass"] ?? null), "html", null, true);
            echo "\" itemscope itemtype=\"http://schema.org/BreadcrumbList\">
            ";
            // line 4
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["breadcrumbs"] ?? null));
            $context['loop'] = [
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            ];
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["b"]) {
                // line 5
                echo "                <li";
                if (((isset($context["itemClass"]) || array_key_exists("itemClass", $context)) && twig_length_filter($this->env, ($context["itemClass"] ?? null)))) {
                    echo " class=\"";
                    echo twig_escape_filter($this->env, ($context["itemClass"] ?? null), "html", null, true);
                    echo "\"";
                }
                echo " itemprop=\"itemListElement\" itemscope itemtype=\"http://schema.org/ListItem\">
                    ";
                // line 6
                if ((twig_get_attribute($this->env, $this->source, $context["b"], "url", []) &&  !twig_get_attribute($this->env, $this->source, $context["loop"], "last", []))) {
                    // line 7
                    echo "                    <a href=\"";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["b"], "url", []), "html", null, true);
                    echo "\" itemprop=\"item\"";
                    if (((isset($context["linkRel"]) || array_key_exists("linkRel", $context)) && twig_length_filter($this->env, ($context["linkRel"] ?? null)))) {
                        echo " rel=\"";
                        echo twig_escape_filter($this->env, ($context["linkRel"] ?? null), "html", null, true);
                        echo "\"";
                    }
                    echo ">
                        ";
                }
                // line 9
                echo "                        <span itemprop=\"name\">";
                if ((twig_get_attribute($this->env, $this->source, $context["b"], "translate", [], "any", true, true) && (twig_get_attribute($this->env, $this->source, $context["b"], "translate", []) == true))) {
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(twig_get_attribute($this->env, $this->source, $context["b"], "text", []), twig_get_attribute($this->env, $this->source, $context["b"], "translationParameters", []), ($context["translation_domain"] ?? null), ($context["locale"] ?? null)), "html", null, true);
                } else {
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["b"], "text", []), "html", null, true);
                }
                echo "</span>
                        ";
                // line 10
                if ((twig_get_attribute($this->env, $this->source, $context["b"], "url", []) &&  !twig_get_attribute($this->env, $this->source, $context["loop"], "last", []))) {
                    // line 11
                    echo "                    </a>
                    ";
                } elseif (twig_get_attribute($this->env, $this->source,                 // line 12
$context["b"], "url", [])) {
                    // line 13
                    echo "                        <meta itemprop=\"item\" content=\"";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["b"], "url", []), "html", null, true);
                    echo "\" />
                    ";
                }
                // line 15
                echo "                    <meta itemprop=\"position\" content=\"";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["loop"], "index", []), "html", null, true);
                echo "\" />

                    ";
                // line 17
                if (( !(null === ($context["separator"] ?? null)) &&  !twig_get_attribute($this->env, $this->source, $context["loop"], "last", []))) {
                    // line 18
                    echo "                        <span class='";
                    echo twig_escape_filter($this->env, ($context["separatorClass"] ?? null), "html", null, true);
                    echo "'>";
                    echo twig_escape_filter($this->env, ($context["separator"] ?? null), "html", null, true);
                    echo "</span>
                    ";
                }
                // line 20
                echo "                </li>
            ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['b'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 22
            echo "        </ol>";
            echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        }
    }

    public function getTemplateName()
    {
        return "@WhiteOctoberBreadcrumbs/microdata.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  127 => 22,  112 => 20,  104 => 18,  102 => 17,  96 => 15,  90 => 13,  88 => 12,  85 => 11,  83 => 10,  74 => 9,  62 => 7,  60 => 6,  51 => 5,  34 => 4,  27 => 3,  25 => 2,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@WhiteOctoberBreadcrumbs/microdata.html.twig", "C:\\SITES\\ragnarstreaming-dbs\\vendor\\whiteoctober\\breadcrumbs-bundle\\WhiteOctober\\BreadcrumbsBundle\\Resources\\views\\microdata.html.twig");
    }
}

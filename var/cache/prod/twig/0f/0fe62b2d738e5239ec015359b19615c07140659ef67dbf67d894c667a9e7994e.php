<?php

/* admin/saisons/edit.html.twig */
class __TwigTemplate_4c62813a443aee10fad44c5ed10a9037a52fe3baa23925d28feb0d00656d7c46 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "admin/saisons/edit.html.twig", 1);
        $this->blocks = [
            'title' => [$this, 'block_title'],
            'meta' => [$this, 'block_meta'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = [])
    {
        echo "Dragon Ball Super Streaming - Admin saison edition";
    }

    // line 3
    public function block_meta($context, array $blocks = [])
    {
        echo "Administration et edition des saisons du site";
    }

    // line 4
    public function block_body($context, array $blocks = [])
    {
        // line 5
        echo "
    <div class=\"container mt-4 mt-container-home\">
        <h2>Editer la ";
        // line 7
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["saison"] ?? null), "name", []), "html", null, true);
        echo "</h2>
        ";
        // line 8
        echo twig_include($this->env, $context, "admin/saisons/_form.html.twig", ["form" => ($context["form"] ?? null), "button" => "Editer"]);
        echo "
    </div>

";
    }

    public function getTemplateName()
    {
        return "admin/saisons/edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  57 => 8,  53 => 7,  49 => 5,  46 => 4,  40 => 3,  34 => 2,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "admin/saisons/edit.html.twig", "C:\\SITES\\ragnarstreaming-dbs\\templates\\admin\\saisons\\edit.html.twig");
    }
}

<?php

use Symfony\Component\Routing\Matcher\Dumper\PhpMatcherTrait;
use Symfony\Component\Routing\RequestContext;

/**
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class srcApp_KernelProdContainerUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    use PhpMatcherTrait;

    public function __construct(RequestContext $context)
    {
        $this->context = $context;
        $this->staticRoutes = [
            '/admin-episodes' => [[['_route' => 'admin.episodes.index', '_controller' => 'App\\Controller\\Admin\\AdminEpisodesController::index'], null, null, null, false, false, null]],
            '/admin/create-episode' => [[['_route' => 'admin.episode.create', '_controller' => 'App\\Controller\\Admin\\AdminEpisodesController::create'], null, null, null, false, false, null]],
            '/admin' => [[['_route' => 'admin.home.index', '_controller' => 'App\\Controller\\Admin\\AdminHomeController::index'], null, null, null, false, false, null]],
            '/admin-saisons' => [[['_route' => 'admin.saisons.index', '_controller' => 'App\\Controller\\Admin\\AdminSaisonsController::index'], null, null, null, false, false, null]],
            '/admin/create-saison' => [[['_route' => 'admin.saison.create', '_controller' => 'App\\Controller\\Admin\\AdminSaisonsController::create'], null, null, null, false, false, null]],
            '/episodes' => [[['_route' => 'episodes', '_controller' => 'App\\Controller\\EpisodesController::index'], null, null, null, false, false, null]],
            '/register' => [[['_route' => 'app_register', '_controller' => 'App\\Controller\\RegistrationController::register'], null, null, null, false, false, null]],
            '/' => [[['_route' => 'home', '_controller' => 'App\\Controller\\SaisonsController::index'], null, null, null, false, false, null]],
            '/series' => [[['_route' => 'series', '_controller' => 'App\\Controller\\SaisonsController::series'], null, null, null, false, false, null]],
            '/adminragnar' => [[['_route' => 'app_login', '_controller' => 'App\\Controller\\SecurityController::login'], null, null, null, false, false, null]],
        ];
        $this->regexpList = [
            0 => '{^(?'
                    .'|/admin/(?'
                        .'|edit\\-(?'
                            .'|episode/([^/]++)(*:42)'
                            .'|link(?'
                                .'|\\-(?'
                                    .'|episode/([^/]++)(*:77)'
                                    .'|saison/([^/]++)(*:99)'
                                .')'
                                .'|Stream\\-(?'
                                    .'|episode/([^/]++)(*:134)'
                                    .'|saison/([^/]++)(*:157)'
                                .')'
                            .')'
                            .'|saison/([^/]++)(*:182)'
                        .')'
                        .'|delete\\-(?'
                            .'|episode/([^/]++)(*:218)'
                            .'|saison/([^/]++)(*:241)'
                        .')'
                    .')'
                    .'|/([a-z0-9\\-]*)\\-([^/]++)/([a-z0-9\\-]*)\\-([^/]++)/([^/]++)(*:308)'
                    .'|/([a-z0-9\\-]*)\\-([^/]++)(*:340)'
                .')/?$}sDu',
        ];
        $this->dynamicRoutes = [
            42 => [[['_route' => 'admin.episode.edit', '_controller' => 'App\\Controller\\Admin\\AdminEpisodesController::edit'], ['id'], ['GET' => 0, 'POST' => 1], null, false, true, null]],
            77 => [[['_route' => 'admin.link.episode.edit', '_controller' => 'App\\Controller\\Admin\\AdminEpisodesController::editLink'], ['id'], ['GET' => 0, 'POST' => 1], null, false, true, null]],
            99 => [[['_route' => 'admin.link.saison.edit', '_controller' => 'App\\Controller\\Admin\\AdminSaisonsController::editLink'], ['id'], ['GET' => 0, 'POST' => 1], null, false, true, null]],
            134 => [[['_route' => 'admin.link.episode.edit.stream', '_controller' => 'App\\Controller\\Admin\\AdminEpisodesController::editlinkStream'], ['id'], ['GET' => 0, 'POST' => 1], null, false, true, null]],
            157 => [[['_route' => 'admin.link.saison.edit.stream', '_controller' => 'App\\Controller\\Admin\\AdminSaisonsController::editlinkStream'], ['id'], ['GET' => 0, 'POST' => 1], null, false, true, null]],
            182 => [[['_route' => 'admin.saison.edit', '_controller' => 'App\\Controller\\Admin\\AdminSaisonsController::edit'], ['id'], ['GET' => 0, 'POST' => 1], null, false, true, null]],
            218 => [[['_route' => 'admin.episode.delete', '_controller' => 'App\\Controller\\Admin\\AdminEpisodesController::delete'], ['id'], ['DELETE' => 0], null, false, true, null]],
            241 => [[['_route' => 'admin.saison.delete', '_controller' => 'App\\Controller\\Admin\\AdminSaisonsController::delete'], ['id'], ['DELETE' => 0], null, false, true, null]],
            308 => [[['_route' => 'episode.show', '_controller' => 'App\\Controller\\EpisodesController::showEpisode'], ['slug_s', 'id_s', 'slug', 'number', 'id'], null, null, false, true, null]],
            340 => [[['_route' => 'saison.show', '_controller' => 'App\\Controller\\SaisonsController::show'], ['slug', 'id'], null, null, false, true, null]],
        ];
    }
}

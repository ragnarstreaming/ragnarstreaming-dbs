<?php

use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Exception\RouteNotFoundException;
use Psr\Log\LoggerInterface;

/**
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class srcApp_KernelProdContainerUrlGenerator extends Symfony\Component\Routing\Generator\UrlGenerator
{
    private static $declaredRoutes;
    private $defaultLocale;

    public function __construct(RequestContext $context, LoggerInterface $logger = null, string $defaultLocale = null)
    {
        $this->context = $context;
        $this->logger = $logger;
        $this->defaultLocale = $defaultLocale;
        if (null === self::$declaredRoutes) {
            self::$declaredRoutes = [
        'admin.episodes.index' => [[], ['_controller' => 'App\\Controller\\Admin\\AdminEpisodesController::index'], [], [['text', '/admin-episodes']], [], []],
        'admin.episode.create' => [[], ['_controller' => 'App\\Controller\\Admin\\AdminEpisodesController::create'], [], [['text', '/admin/create-episode']], [], []],
        'admin.episode.edit' => [['id'], ['_controller' => 'App\\Controller\\Admin\\AdminEpisodesController::edit'], [], [['variable', '/', '[^/]++', 'id', true], ['text', '/admin/edit-episode']], [], []],
        'admin.link.episode.edit' => [['id'], ['_controller' => 'App\\Controller\\Admin\\AdminEpisodesController::editLink'], [], [['variable', '/', '[^/]++', 'id', true], ['text', '/admin/edit-link-episode']], [], []],
        'admin.link.episode.edit.stream' => [['id'], ['_controller' => 'App\\Controller\\Admin\\AdminEpisodesController::editlinkStream'], [], [['variable', '/', '[^/]++', 'id', true], ['text', '/admin/edit-linkStream-episode']], [], []],
        'admin.episode.delete' => [['id'], ['_controller' => 'App\\Controller\\Admin\\AdminEpisodesController::delete'], [], [['variable', '/', '[^/]++', 'id', true], ['text', '/admin/delete-episode']], [], []],
        'admin.home.index' => [[], ['_controller' => 'App\\Controller\\Admin\\AdminHomeController::index'], [], [['text', '/admin']], [], []],
        'admin.saisons.index' => [[], ['_controller' => 'App\\Controller\\Admin\\AdminSaisonsController::index'], [], [['text', '/admin-saisons']], [], []],
        'admin.saison.create' => [[], ['_controller' => 'App\\Controller\\Admin\\AdminSaisonsController::create'], [], [['text', '/admin/create-saison']], [], []],
        'admin.saison.edit' => [['id'], ['_controller' => 'App\\Controller\\Admin\\AdminSaisonsController::edit'], [], [['variable', '/', '[^/]++', 'id', true], ['text', '/admin/edit-saison']], [], []],
        'admin.link.saison.edit' => [['id'], ['_controller' => 'App\\Controller\\Admin\\AdminSaisonsController::editLink'], [], [['variable', '/', '[^/]++', 'id', true], ['text', '/admin/edit-link-saison']], [], []],
        'admin.link.saison.edit.stream' => [['id'], ['_controller' => 'App\\Controller\\Admin\\AdminSaisonsController::editlinkStream'], [], [['variable', '/', '[^/]++', 'id', true], ['text', '/admin/edit-linkStream-saison']], [], []],
        'admin.saison.delete' => [['id'], ['_controller' => 'App\\Controller\\Admin\\AdminSaisonsController::delete'], [], [['variable', '/', '[^/]++', 'id', true], ['text', '/admin/delete-saison']], [], []],
        'episodes' => [[], ['_controller' => 'App\\Controller\\EpisodesController::index'], [], [['text', '/episodes']], [], []],
        'episode.show' => [['slug_s', 'id_s', 'slug', 'number', 'id'], ['_controller' => 'App\\Controller\\EpisodesController::showEpisode'], ['slug' => '[a-z0-9\\-]*', 'slug_s' => '[a-z0-9\\-]*'], [['variable', '/', '[^/]++', 'id', true], ['variable', '-', '[^/]++', 'number', true], ['variable', '/', '[a-z0-9\\-]*', 'slug', true], ['variable', '-', '[^/]++', 'id_s', true], ['variable', '/', '[a-z0-9\\-]*', 'slug_s', true]], [], []],
        'app_register' => [[], ['_controller' => 'App\\Controller\\RegistrationController::register'], [], [['text', '/register']], [], []],
        'home' => [[], ['_controller' => 'App\\Controller\\SaisonsController::index'], [], [['text', '/']], [], []],
        'series' => [[], ['_controller' => 'App\\Controller\\SaisonsController::series'], [], [['text', '/series']], [], []],
        'saison.show' => [['slug', 'id'], ['_controller' => 'App\\Controller\\SaisonsController::show'], ['slug' => '[a-z0-9\\-]*'], [['variable', '-', '[^/]++', 'id', true], ['variable', '/', '[a-z0-9\\-]*', 'slug', true]], [], []],
        'app_login' => [[], ['_controller' => 'App\\Controller\\SecurityController::login'], [], [['text', '/adminragnar']], [], []],
    ];
        }
    }

    public function generate($name, $parameters = [], $referenceType = self::ABSOLUTE_PATH)
    {
        $locale = $parameters['_locale']
            ?? $this->context->getParameter('_locale')
            ?: $this->defaultLocale;

        if (null !== $locale && null !== $name) {
            do {
                if ((self::$declaredRoutes[$name.'.'.$locale][1]['_canonical_route'] ?? null) === $name) {
                    unset($parameters['_locale']);
                    $name .= '.'.$locale;
                    break;
                }
            } while (false !== $locale = strstr($locale, '_', true));
        }

        if (!isset(self::$declaredRoutes[$name])) {
            throw new RouteNotFoundException(sprintf('Unable to generate a URL for the named route "%s" as such route does not exist.', $name));
        }

        list($variables, $defaults, $requirements, $tokens, $hostTokens, $requiredSchemes) = self::$declaredRoutes[$name];

        return $this->doGenerate($variables, $defaults, $requirements, $tokens, $parameters, $name, $referenceType, $hostTokens, $requiredSchemes);
    }
}
